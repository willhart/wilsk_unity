# WILSK UNITY

> Version 0.1.0

This is a simple Unity3d project base. It includes some default scripts which make up
the skeleton of a game, with some commonly required tasks implemented. There is a "sandbox" 
scene with some suggested configuration to act as a starting point.

Most scripts are in the `Wilsk` namespace and are written in C# and at this point are 
poorly tested!

## Requirements

WilskUnity requires the following packages:

- [Unity Test Tools](https://www.assetstore.unity3d.com/en/#!/content/13802) free from the asset store - exclude the examples directory
- [Zenject](https://github.com/modesttree/Zenject) or free from the asset store
- Visual Studio integration (I use VS2015 Community)

## Features

- Dependency Injection via Zenject
- Simple A* Pathfinding for 2D or 3D games using a custom grid
- Asynchronous (threaded) or synchronous (immediate) pathfinding
- Base Enemy class providing a way to spawn multiple enemy types from one factory
- Input Proxies, which allow drop-in control configurations, including sample top down and platformer control schemes

## Usage

Clone or download the repository and use this as a starting point. There
is an example scene showing the usage of dependency injection and pathfinding.
All required scripts are in the `Core` folder. Other folders can be removed 
if they are not required although there may be minor compilation errors which 
need to be fixed which can usually just be done by removing the lines that are
raising errors.

If you remove a component you will also have to remove any dependency injection
for it. This can be done by opening `Core/DependencyInstaller.cs` and commenting
out or deleting the `#define` corresponding to the components that have been removed.

## Available Components

### Path Finding

The set up for pathfinding is as follows (see the sandbox for an example)

1. Create an empty prefab called "Pathfinding" - the name doesn't really matter though
2. Add a `Graph` component to the prefab
3. Position the "Pathfinding" component at the bottom left of your game area (i.e. at the bottom left of the map)
4. Set the `World Size` parameter so that the pathfinding grid matches the game map.
5. Set the `Node Radius` to suit - lower numbers result in longer calculations but more precise path finding. A good default is `0.25`. 

The graph can operate in two modes - 3d (top down or on the XZ plane) or 2d (on the XY plane). 
If you want to run in Unity's 2d mode, set `Is 2d Mode` to checked, otherwise leave it unchecked. 

There are several ways that a graph can be generated.  At the moment only two are supported - 
`Empty` and `Raycast`.  `Empty` graphs assume everywhere is walkable, while `Raycast` ones use 
collisions with objects to determine what is walkable and what isn't. 

> Loading graphs from file is a planned feature

The following instructions are for setting up a `Raycast` graph generator:

1. Create a layer for unwalkable objects - potentially called "Unwalkable" 
2. Create a bunch of obstacles and add them to this layer
3. In the `Graph` component on the `Pathfinding` gameobject, set the GraphBuilder to `Raycast` and the Unwalkable Layer to `Unwalkable` (or whatever you named the layer above)

### Entities

The `Wilsk.Entities` namespace contains a number of useful tools for creating "enemies" or "player" 
objects including an example of a `BaseEnemy` class with a spawn factory following the Zenject recommended 
pattern. 

For enemies with common attributes and behaviours (i.e. ones that can be described by a single base class),
a single `BaseEnemy` factory is provided. Attributes of different enemies are provided by modifying the 
`EnemyTemplate` class inside `EntitySettings`. The simple process for defining enemies is:

1. Add an enemy template in the Unity Inspector on the EntityDependencyInstaller settings and populate the details
2. Add an enum item to match in the `EnemyType` enum

The `BaseEnemyFactory` can be injected into any class to enable it to spawn enemies. The syntax for creating enemies
is 

    _factory.Create(EnemyType.MyEnemyType)

Alternatively, a `BaseEnemySpawner` MonoBehaviour is provided which can be added to a MonoBehaviour. If enemy behaviours
differ significantly then a new enemy class should be developed.

### Input

The `Wilsk.Input` namespace includes a number of Input Proxies. These are used to abstract and encapsulate 
the gathering of player input. A specific "rule set", i.e. the `PlatformerInputProxy` can be injected to 
apply a certain set of controls by modifying the `InputDependencyInstaller` class.

> Inputs must first be set up in the usual Unity Input settings section. 