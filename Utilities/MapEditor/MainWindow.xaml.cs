﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Wilsk.MapEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MapDefinition map;

        public MainWindow()
        {
            InitializeComponent();

            HeightToSetTextBox.Text = EditorActionProperties.DeltaHeight.ToString();
        }
        
        /// <summary>
        /// Warn if the text box isn't an integer value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HeightToSetTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            byte h;

            if (!byte.TryParse(HeightToSetTextBox.Text, out h))
            {
                MessageBox.Show("Please set a numeric value between 0 and 255 in the height text box");
            }
            else
            {
                EditorActionProperties.DeltaHeight = h;
            }

            HeightToSetTextBox.Text = EditorActionProperties.DeltaHeight.ToString();
        }

        private void NewMapButton_Click(object sender, RoutedEventArgs e)
        {
            map = new MapDefinition(100, 100, MapCanvas);
            map.BuildGrid(MapCanvas, (int)MapCanvas.RenderSize.Width, (int)MapCanvas.RenderSize.Height);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (map != null)
            {
                map.BuildGrid(MapCanvas, (int)MapCanvas.RenderSize.Width, (int)MapCanvas.RenderSize.Height);
            }
        }

        private void Window_KeyStateChanged(object sender, KeyEventArgs e)
        {
            // press S to subtract height
            if (e.Key == Key.S)
            {
                EditorActionProperties.IsIncreasingHeight = !e.IsDown;
            }

            // press A to flood fill an area
            if (e.Key == Key.A)
            {
                EditorActionProperties.IsFloodFilling = e.IsDown;
            }

            // Press D to draw where the cursor moves
            if (e.Key == Key.D)
            {
                EditorActionProperties.IsDrawing = e.IsDown;
            }

            // press F to flatten out terrain
            if (e.Key == Key.F)
            {
                EditorActionProperties.IsFlattening = e.IsDown;
                if (!EditorActionProperties.IsFlattening) EditorActionProperties.FlattenHeight = null; // reset height
            }

            UpdateActionLabel();
        }

        /// <summary>
        /// Export the map to file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            if (map == null)
            {
                return;
            }

            var sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == true)
            {
                var mapStr = map.ToString();
                File.WriteAllText(sfd.FileName, mapStr);
            }
        }

        /// <summary>
        /// Loads a map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadMapButton_Click(object sender, RoutedEventArgs e)
        {
            var fd = new OpenFileDialog();
            if (fd.ShowDialog() == true)
            {
                var path = fd.FileName;

                map = new MapDefinition(100, 100, MapCanvas);
                map.Load(path);

                map.BuildGrid(MapCanvas, (int)MapCanvas.RenderSize.Width, (int)MapCanvas.RenderSize.Height);
            }
        }

        /// <summary>
        /// Sets the action label text describing what the user is currently doing
        /// </summary>
        private void UpdateActionLabel()
        {
            CurrentActionLabel.Content = EditorActionProperties.GetStateDescription();
        }

        private void MapCanvas_MouseStateChanged(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                EditorActionProperties.LeftMouseDown = e.ButtonState == MouseButtonState.Pressed;
            }
        }
    }
}
