﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilsk.MapEditor
{
    internal static class EditorActionProperties
    {
        public static byte DeltaHeight = 5;

        public static bool IsIncreasingHeight = true;

        public static bool IsFloodFilling = false;

        public static bool IsDrawing = false;

        public static bool IsFlattening = false;

        public static byte? FlattenHeight = null;

        public static bool LeftMouseDown = false;

        public static string GetStateDescription()
        {
            var sb = new List<string>();

            if (LeftMouseDown)
            {
                sb.Add("+");
            }
            else
            {
                sb.Add("o");
            }

            if (!IsFlattening ||
                IsFloodFilling ||
                IsDrawing)
            {
                if (IsIncreasingHeight)
                {
                    sb.Add("INCREASE");
                }
                else
                {
                    sb.Add("DECREASE");
                }
            }

            if (IsFloodFilling)
            {
                sb.Add("FILL");
            }
            else if (IsDrawing)
            {
                sb.Add("DRAW");
            }
            else if (IsFlattening)
            {
                sb.Add("FLATTEN");

                if (FlattenHeight != null)
                {
                    sb.Add("TO " + FlattenHeight.ToString());
                }
            }
            else
            {
                sb.Add("POINT");
            }

            return string.Join(" ", sb);
        }
    }
}
