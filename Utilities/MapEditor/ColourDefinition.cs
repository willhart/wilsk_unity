﻿namespace Wilsk.MapEditor
{
    using System.Collections.Generic;
    using System.Windows.Media;

    internal static class ColourDefinition
    {
        private static List<float> _dictKeys = new List<float>()
        {
            0, 0.05f, 0.3f, 0.4f, 0.8f, 0.9f, 1f
        };

        private static Dictionary<float, Color> _colours = new Dictionary<float, Color>()
        {
            { _dictKeys[0], Color.FromRgb(75, 132, 255) },
            { _dictKeys[1], Color.FromRgb(255, 254, 128) },
            { _dictKeys[2], Color.FromRgb(178, 255, 88) },
            { _dictKeys[3], Color.FromRgb(210, 255, 111) },
            { _dictKeys[4], Colors.Gray },
            { _dictKeys[5], Color.FromRgb(77, 77, 77) },
            { _dictKeys[6], Colors.Snow }
        };

        /// <summary>
        /// Gets a colour based on the 0-1 proportion of the height range that this
        /// level represents
        /// </summary>
        /// <param name="proportion"></param>
        /// <returns></returns>
        internal static Color GetColour(float proportion)
        {
            // handle exact matches
            var idx = _dictKeys.IndexOf(proportion);
            if (idx >= 0)
            {
                return _colours[_dictKeys[idx]];
            }

            // find bracketing indices
            var prior = 0;

            for (var i = prior; i < _dictKeys.Count - 2; ++i)
            {
                if (proportion < _dictKeys[i + 1])
                {
                    break;
                }

                ++prior;
            }

            return ColorLerp(_colours[_dictKeys[prior]], _colours[_dictKeys[prior + 1]], proportion);
        }

        private static Color ColorLerp(Color col, Color other, float proportion)
        {
            var r = (byte)Interpolate(col.R, other.R, proportion);
            var g = (byte)Interpolate(col.G, other.G, proportion);
            var b = (byte)Interpolate(col.B, other.B, proportion);

            return Color.FromRgb(r, g, b);
        }

        private static float Interpolate(float from, float to, float proportion)
        {
            return proportion * (to - from) + from;
        }
    }
}
