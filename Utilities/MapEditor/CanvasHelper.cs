﻿namespace Wilsk.MapEditor
{
    using System.Windows.Controls;

    internal static class CanvasHelper
    {
        public static void BuildGrid(Canvas canvas, NodeDefinition[,] nodes, float tileSize)
        {
            canvas.Children.Clear();

            var xMax = nodes.GetLength(0);
            var yMax = nodes.GetLength(1);

            // traverse nodes and add to canvas
            for (var x = 0; x < xMax; ++x)
            {
                for (var y = 0; y < yMax; ++y)
                {
                    nodes[x, y].Update(tileSize);
                    var r = nodes[x, y].Rect;
                    canvas.Children.Add(r);
                    Canvas.SetLeft(r, x * tileSize);
                    Canvas.SetBottom(r, y * tileSize);
                }
            }
        }
    }
}
