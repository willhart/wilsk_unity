﻿namespace Wilsk.MapEditor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Controls;

    public class MapDefinition
    {
        private float _tileSize;

        private Canvas _canvas;

        public MapDefinition(int width, int height, Canvas canvas)
        {
            _canvas = canvas;

            NumberOfXNodes = width;
            NumberOfYNodes = height;

            _tileSize = Math.Min((int)canvas.RenderSize.Width / width, (int)canvas.RenderSize.Height / height);

            Grid = new NodeDefinition[width, height];

            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    var node =  new NodeDefinition(_tileSize) { X = x, Y = y, Height = 0, MovementCost = 1, Walkable = true };
                    node.FloodFill += Node_FloodFill;
                    Grid[x, y] = node;
                }
            }

            MaxHeight = 0;
            MinHeight = 0;
        }

        /// <summary>
        /// Flood fills from the given node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Node_FloodFill(object sender, EventArgs e)
        {
            var rootNode = sender as NodeDefinition;
            var fillHeight = rootNode.Height;
            var newHeight = NodeDefinition.IncrementHeightSafely(fillHeight, EditorActionProperties.DeltaHeight);

            // start the flood fill
            PerformFloodFill(rootNode.X, rootNode.Y, fillHeight, newHeight);
        }

        /// <summary>
        /// Handles the flood fill recursively
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="startHeight"></param>
        /// <param name="setHeight"></param>
        private void PerformFloodFill(int x, int y, byte startHeight, byte setHeight)
        {
            if (Grid[x, y].Height != startHeight) return;

            var q = new Queue<NodeDefinition>();
            q.Enqueue(Grid[x, y]);

            while(q.Count > 0)
            {
                // pick a node
                var N = q.Dequeue();
                N.Height = setHeight;
                N.Update();

                // add surrounding nodes to queue
                CheckNodeForFill(N.X - 1, N.Y, startHeight, q);
                CheckNodeForFill(N.X + 1, N.Y, startHeight, q);
                CheckNodeForFill(N.X, N.Y + 1, startHeight, q);
                CheckNodeForFill(N.X, N.Y - 1, startHeight, q);
            }
        }

        private void CheckNodeForFill(int x, int y, byte startHeight, Queue<NodeDefinition> q)
        {
            if (CoordinatesOnMap(x, y) && Grid[x, y].Height == startHeight)
            {
                var n = Grid[x, y];

                if (!q.Contains(n))
                {
                    q.Enqueue(n);
                }
            }
        }

        /// <summary>
        /// Check if the given x-y coordinates are on the map
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private bool CoordinatesOnMap(int x, int y)
        {
            return x >= 0 && x < NumberOfXNodes &&
                y >= 0 && y < NumberOfYNodes;
        }

        /// <summary>
        /// Redraws the grid lines when something has changed (i.e. window resized)
        /// </summary>
        public void BuildGrid(Canvas canvas, int mapRenderWidth, int mapRenderHeight)
        {
            _tileSize = Math.Min(mapRenderWidth / NumberOfXNodes, mapRenderHeight / NumberOfYNodes);
            CanvasHelper.BuildGrid(canvas, Grid, _tileSize);
        }

        /// <summary>
        /// Loads a graph from string
        /// </summary>
        /// <param name="path"></param>
        public void Load(string path)
        {
            var txt = File.ReadAllText(path);
            var g = new MapEditorGraphParser(txt);

            MaxHeight = byte.MinValue;
            MinHeight = byte.MaxValue;

            // parse
            Grid = g.BuildGraph();
            var hm = g.BuildHeightMap();

            // Apply the height map
            for (var x = 0; x < hm.GetLength(0); ++x)
            {
                for (var y = 0; y < hm.GetLength(1); ++y)
                {
                    var h = hm[x, y];
                    if (h > MaxHeight) MaxHeight = h;
                    if (h < MinHeight) MinHeight = h;

                    Grid[x, y].Height = h;
                    Grid[x, y].FloodFill += Node_FloodFill;
                }
            }
        }

        /// <summary>
        /// Builds string representation of the graph suitable for file saving
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb_walkable = new StringBuilder();
            var sb_hm = new StringBuilder();

            // add the size line
            sb_walkable.Append(string.Format("{0}x{1}{2}", NumberOfXNodes, NumberOfYNodes, Environment.NewLine));

            // build walkable map
            for (var y = 0; y < NumberOfYNodes; ++y)
            {
                var walkables = new List<int>();
                var hms = new List<byte>();

                for (var x = 0; x < NumberOfXNodes; ++x)
                {
                    walkables.Add(Grid[x, y].Walkable ? 0 : -1);
                    hms.Add(Grid[x, y].Height);
                }

                sb_walkable.Append(string.Join(",", walkables));
                sb_walkable.Append(Environment.NewLine);

                sb_hm.Append(string.Join(",", hms));
                sb_hm.Append(Environment.NewLine);
            }

            sb_walkable.Append(Environment.NewLine);
            sb_walkable.Append(sb_hm);
            
            return sb_walkable.ToString().TrimEnd();
        }

        public NodeDefinition[,] Grid
        {
            get; set;
        }

        public int MinHeight
        {
            get; set;
        }

        public int MaxHeight
        {
            get; set;
        }

        public int NumberOfXNodes { get; private set; }

        public int NumberOfYNodes { get; private set; }
    }
}
