﻿namespace Wilsk.MapEditor
{
    using System;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;

    public class NodeDefinition
    {
        public event EventHandler FloodFill;

        private byte _height;
        private bool _walkable;
        private float _nodeSize;
        private Rectangle _rectangle;
        
        public NodeDefinition(float nodeSize)
        {
            _nodeSize = nodeSize;
        }

        public void Update()
        {
            if (_rectangle == null)
            {
                _rectangle = new Rectangle();
                _rectangle.MouseDown += HandleNodeMouseDown;
                _rectangle.MouseEnter += HandleMouseEnter;
            }

            _rectangle.Width = _nodeSize;
            _rectangle.Height = _nodeSize;
            _rectangle.Stroke = Walkable ? Brushes.DarkGray : Brushes.DarkMagenta;
            _rectangle.Fill = new SolidColorBrush(ColourDefinition.GetColour((float)Height / 255));
        }

        public void Update(float newNodeSize)
        {
            _nodeSize = newNodeSize;
            Update();
        }

        private void HandleMouseEnter(object sender, MouseEventArgs e)
        {
            if (EditorActionProperties.LeftMouseDown)
            {
                if (EditorActionProperties.IsDrawing)
                {
                    Height = IncrementHeightSafely(Height, EditorActionProperties.DeltaHeight);
                    Update();
                }
                else if (EditorActionProperties.IsFlattening)
                {
                    Height = (byte)EditorActionProperties.FlattenHeight;
                    Update();
                }
            }
        }

        private void HandleNodeMouseDown(object sender, MouseButtonEventArgs e)
        {
            // handle the click
            if (e.ChangedButton == MouseButton.Left)
            {
                if (EditorActionProperties.IsFloodFilling)
                {
                    if (FloodFill != null)
                    {
                        FloodFill(this, new EventArgs());
                    }
                    return;
                }
                else if (EditorActionProperties.IsFlattening)
                {
                    if (EditorActionProperties.FlattenHeight == null)
                    {
                        EditorActionProperties.FlattenHeight = Height;
                    }
                }
                else
                {
                    // change height, preventing overflow
                    Height = IncrementHeightSafely(Height, EditorActionProperties.DeltaHeight);
                    Update();
                }
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                // toggle walkable
                Walkable = !Walkable;
                Update();
            }
        }

        /// <summary>
        /// Safely determines a height to set given the current height and the delta height. 
        /// Uses the main window "IncreasingHeight" property to determine direction
        /// </summary>
        /// <param name="currentHeight"></param>
        /// <param name="deltaHeight"></param>
        /// <returns></returns>
        public static byte IncrementHeightSafely(byte currentHeight, byte deltaHeight)
        {
            if (
                (EditorActionProperties.IsIncreasingHeight && (byte.MaxValue - deltaHeight) < currentHeight) ||
                (!EditorActionProperties.IsIncreasingHeight && deltaHeight > currentHeight)
            )
            {
                return EditorActionProperties.IsIncreasingHeight ? byte.MaxValue : byte.MinValue;
            }
            else
            {
                if (EditorActionProperties.IsIncreasingHeight)
                {
                    return (byte)(currentHeight + deltaHeight);
                }
                else
                {
                    return (byte)(currentHeight - deltaHeight);
                }
            }
        }

        public byte Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        public bool Walkable
        {
            get
            {
                return _walkable;
            }
            set
            {
                _walkable = value;
            }
        }

        public Rectangle Rect
        {
            get
            {
                return _rectangle;
            }
        }

        public int MovementCost { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
    }
}
