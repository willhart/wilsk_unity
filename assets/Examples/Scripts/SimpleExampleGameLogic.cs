﻿namespace Wilsk.Example.Logic
{
    using UnityEngine;
    using Zenject;

    using Entities;
    using Core.Logic;
    using Core.Messaging;

    /// <summary>
    /// Some sample logic for the simple example game
    /// </summary>
    public class SimpleExampleGameLogic : AbstractGameLogic
    {
#pragma warning disable 0649
        /// <summary>
        /// A factory for creating enemies
        /// </summary>
        [Inject]
        private LocalEnemy.Factory _factory;
#pragma warning restore 0649

        /// <summary>
        /// The last time an enemy was spawned
        /// </summary>
        private float _lastSpawnedTime;
        private bool _spawned = false;

        private static readonly float spawnDelay = 1;

        public SimpleExampleGameLogic() : base()
        {
            _lastSpawnedTime = Time.time; // prevent immediate spawn
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args) { }

        public override void Subscribe() { }

        public override void Update(float deltaTime)
        {
            if (_spawned) return;

            var timeSinceLastUpdate = Time.time - _lastSpawnedTime;
            if (timeSinceLastUpdate > spawnDelay)
            {
                _spawned = true;
                _lastSpawnedTime = Time.time;

                var e1 = _factory.Create(EnemyType.Infantry, new Vector3(10, 10, -20f - 0.5f));
                var e2 = _factory.Create(EnemyType.Motorised, new Vector3(80, 80, -20f - 0.5f));
            }
        }
    }
}
