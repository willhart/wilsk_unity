﻿namespace Wilsk.Example.Screens
{
    using Zenject;

    using Core;
    using Core.Logic;
    using Core.Screens;
    using Entities;

    /// <summary>
    /// An inheritable base class for all levels
    /// </summary>
    public class SimpleGameExampleMainScreen : AbstractScreen
    {
#pragma warning disable 0649
        /// <summary>
        /// The game logic to apply for this scene
        /// </summary>
        [Inject]
        private IGameLogic _logic;

        /// <summary>
        /// Manages visibility between visible components
        /// </summary>
        [Inject]
        private VisibilityManager _visibilityManager;
#pragma warning restore 0649
        
        public SimpleGameExampleMainScreen(Game game) : base(game) { }
        
        public override void Update()
        {
            base.Update();
            _visibilityManager.Update();
            _logic.Update(TimeKeeper.DeltaTime);
        }

        /// <summary>
        /// Gets a unique string which identifies the level. Will be validated as
        /// unique at run time.
        /// </summary>
        public override ScreenType ScreenType
        {
            get
            {
                return ScreenType.GameLevel;
            }
        }

        /// <summary>
        /// Gets the scene name for loading in the level
        /// </summary>
        protected override string SceneName
        {
            get
            {
                return "SimpleGameExampleScreenScene";
            }
        }
    }
}
