﻿namespace Wilsk.Editor
{
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Adapted from https://github.com/pharan/Unity-MeshSaver
    /// </summary>
    public static class MeshSaverEditor
    {

        [MenuItem("CONTEXT/MeshFilter/Save Mesh...")]
        public static void SaveMeshInPlace(MenuCommand menuCommand)
        {
            var mf = menuCommand.context as MeshFilter;
            var m = mf.sharedMesh;
            SaveMesh(m, m.name, false);
        }

        [MenuItem("CONTEXT/MeshFilter/Save Mesh As New Instance...")]
        public static void SaveMeshNewInstanceItem(MenuCommand menuCommand)
        {
            var mf = menuCommand.context as MeshFilter;
            var m = mf.sharedMesh;
            SaveMesh(m, m.name, true);
        }

        public static void SaveMesh(Mesh mesh, string name, bool makeNewInstance)
        {
            string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/", name, "asset");
            if (string.IsNullOrEmpty(path)) return;

            path = FileUtil.GetProjectRelativePath(path);

            var meshToSave = (makeNewInstance) ? Object.Instantiate(mesh) as Mesh : mesh;
            meshToSave.Optimize();

            AssetDatabase.CreateAsset(meshToSave, path);
            AssetDatabase.SaveAssets();
        }

    }
}