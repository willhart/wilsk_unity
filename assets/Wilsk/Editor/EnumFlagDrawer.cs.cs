﻿using UnityEditor;
using UnityEngine;

using Wilsk.Core;

/// <summary>
/// A simple property drawer for laying out enum flags as toggle buttons
/// Based on http://www.sharkbombs.com/2015/02/17/unity-editor-enum-flags-as-toggle-buttons/
/// 
/// Updated to allow multiple rows
/// </summary>
[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagsAttributeDrawer : PropertyDrawer
{
    private static readonly float RowHeight = 20f;

    private static readonly int ButtonsPerRow = 2;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var enumLength = property.enumNames.Length;
        var rows = Mathf.CeilToInt(enumLength / ButtonsPerRow) + 1;
        return rows * RowHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int buttonsIntValue = 0;
        int enumLength = property.enumNames.Length;
        bool[] buttonPressed = new bool[enumLength];
        float buttonWidth = (position.width - EditorGUIUtility.labelWidth) / ButtonsPerRow;

        EditorGUI.LabelField(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height), label);

        EditorGUI.BeginChangeCheck();

        for (int i = 0; i < enumLength; i++)
        {
            int row = Mathf.FloorToInt(i / ButtonsPerRow);

            // Check if the button is/was pressed
            if ((property.intValue & (1 << i)) == 1 << i)
            {
                buttonPressed[i] = true;
            }

            Rect buttonPos = new Rect(
                position.x + EditorGUIUtility.labelWidth + (i % ButtonsPerRow) * buttonWidth, 
                position.y + row * RowHeight, 
                buttonWidth, RowHeight);

            buttonPressed[i] = GUI.Toggle(buttonPos, buttonPressed[i], property.enumNames[i], "Button");

            if (buttonPressed[i])
                buttonsIntValue += 1 << i;
        }

        if (EditorGUI.EndChangeCheck())
        {
            property.intValue = buttonsIntValue;
        }
    }
}
