﻿namespace Wilsk.Graph.Tests
{
    using NUnit.Framework;
    using System;
    using UnityEngine;

    [TestFixture]
    [Category("Wilsk.Entity.Graph Tests")]
    public class GraphTests
    {   
        private int _sizeX = 10;

        private int _sizeY = 10;

        private StringGraphBuilder _sgb;

        private byte[,] _hm;

        private Node[,] _graph;

        [TestFixtureSetUp]
        public void Setup()
        {
            string g = @"10x10
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0

0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,5,5,5,5,0,0,0
0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0";
            _sgb = new StringGraphBuilder();
            _sgb.Configure(new LayerMask(), g);
            _hm = _sgb.BuildHeightMap(_sizeX, _sizeY);
            _graph = _sgb.BuildGraph(_sizeX, _sizeY, 2, true, (a, b) => { return Vector3.zero; });
        }

        [Test]
        public void LineOfSightOnFlatGroundDiagonallyTest()
        {
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[0, 0], _graph[9, 1]));
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[3, 3], _graph[6, 4]));
        }

        [Test]
        public void LineOfSightOnFlatGroundStraightLineTest()
        {
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[0, 0], _graph[9, 0]));
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[3, 3], _graph[3, 3]));
        }

        [Test]
        public void NoLineOfSightOnFlatGroundDiagonallyTest()
        {
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[1, 3], _graph[4, 3])); // along x
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[5, 0], _graph[4, 4])); // along y
        }

        [Test]
        public void NoLineOfSightOnFlatGroundStraightLineTest()
        {
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[0, 3], _graph[4, 3])); // along x
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[4, 0], _graph[4, 4])); // along y
        }

        [Test]
        public void LineOfSightToUnitsOnCliffEdgesTest()
        {
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[0, 3], _graph[3, 4]));
            Assert.IsTrue(Graph.InLineOfSight(_hm, _graph[3, 4], _graph[2, 3]));
        }

        [Test]
        public void NoLineOfSightToUnitsAtCliffBasesTest()
        {
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[4, 3], _graph[2, 4]));
        }

        [Test]
        public void ObstaclesBlockLineOfSightTest()
        {
            Assert.IsFalse(Graph.InLineOfSight(_hm, _graph[0, 0], _graph[9, 9]));
        }
    }
}
