﻿namespace Wilsk.Graph.Tests
{
    using NUnit.Framework;
    using System;
    using UnityEngine;

    [TestFixture]
    [Category("Wilsk.Entity.Graph Tests")]
    internal class StringGraphBuilderTest
    {
        private string _simpleGraph = @"3x2
-1,0,1
0,-1,0

1,2,3
4,5,6";
        private string _invalidGraph = @"3x2
z,0,1
1,23,5


1,2,3
4,5,6";

        private string _errorGraph = @"3x2
z,0,1


1,2,3,4";

        private int _sizeX = 3;

        private int _sizeY = 2;

        [Test]
        public void LoadGraphFromStringTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), _simpleGraph);
            var nodes = sgb.BuildGraph(
                _sizeX, _sizeY, 2, true, 
                (a, b) => { return new Vector3(1, 2, 3); });

            Assert.AreEqual(_sizeX, nodes.GetLength(0));
            Assert.AreEqual(_sizeY, nodes.GetLength(1));

            Assert.AreEqual(false, nodes[0, 0].Walkable);
            Assert.AreEqual(false, nodes[1, 1].Walkable);

            Assert.AreEqual(true, nodes[1, 0].Walkable);
            Assert.AreEqual(true, nodes[2, 0].Walkable);
            Assert.AreEqual(true, nodes[0, 1].Walkable);
            Assert.AreEqual(true, nodes[2, 1].Walkable);
        }

        [Test]
        public void LoadHeightMapFromStringTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), _simpleGraph);

            var hmExpected = new byte[,]
            {
                { 1, 4 },
                { 2, 5 },
                { 3, 6 }
            };

            var hmFound = sgb.BuildHeightMap(3, 2);

            Assert.AreEqual(hmExpected, hmFound);
        }

        [Test]
        public void LoadGraphWithInvalidEntryDefaultsToUnwalkableTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), _invalidGraph);
            var nodes = sgb.BuildGraph(
                _sizeX, 1, 2, true,
                (a, b) => { return new Vector3(1, 2, 3); });

            Assert.AreEqual(false, nodes[0, 0].Walkable);
        }
        
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidSizeStringRaisesArgumentExceptionTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), @"ax2");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void MissingSizeStringRaisesArgumentExceptionTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), @"1,2,3
1,2,3

1,2,3
1,2,3");
        }

#pragma warning disable 0168

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void LoadGraphWithInvalidFileRaisesExceptionTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), _errorGraph);
            var nodes = sgb.BuildGraph(
                _sizeX * 2, _sizeY, 2, true,
                (a, b) => { return new Vector3(1, 2, 3); });
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void InvalidNumberOfLinesOnHeightMapLoadCausesExceptionTest()
        {
            var sgb = new StringGraphBuilder();
            sgb.Configure(new LayerMask(), _errorGraph);
            var nodes = sgb.BuildHeightMap(_sizeX, _sizeY);
        }
#pragma warning restore 0168
    }
}
