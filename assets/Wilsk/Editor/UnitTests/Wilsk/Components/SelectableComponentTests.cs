﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using System.Linq;

    using Core.Messaging;
    using Entities;

    using UnityEngine;

    [TestFixture]
    [Category("Wilsk.Component.SelectableComponent Tests")]
    internal class SelectableComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {
        private SelectableComponent _sc;

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _receivedMessages.Clear();

            if (_sc != null)
            {
                _bus.Unsubscribe(_sc);
            }

            _sc = new SelectableComponent(_tmpl, _bus);
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetSelected);
        }

        [Test]
        public void SelectionStartsAsFalseTest()
        {
            Assert.IsFalse(_sc.IsSelected);
        }

        [Test]
        public void SelectionIsSetToTrueTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentOnLeftClick, null);
            Assert.IsTrue(_sc.IsSelected);
            Assert.AreEqual(1, _receivedMessages.Count);
            Assert.AreEqual(true, _receivedMessages.First().Value.Get<bool>());
        }

        [Test]
        public void SelectionIsSetToFalseTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentOnLeftClick, null);

            _receivedMessages.Clear();

            _bus.SendMessage(this, MessageBusMessageType.ComponentOnLeftClick, null);

            Assert.IsFalse(_sc.IsSelected);
            Assert.AreEqual(1, _receivedMessages.Count);
            Assert.AreEqual(false, _receivedMessages.First().Value.Get<bool>());
        }

        [Test]
        public void SetSelection()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentSetSelected, true);
            Assert.IsTrue(_sc.IsSelected);

            _receivedMessages.Clear();
            _bus.SendMessage(this, MessageBusMessageType.ComponentSetSelected, false);
            Assert.IsFalse(_sc.IsSelected);
        }
    }
}
