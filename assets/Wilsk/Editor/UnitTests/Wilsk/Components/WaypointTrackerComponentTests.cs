﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using UnityEngine;

    using Core.Messaging;
    using Components;
    using Entities;
    
    [TestFixture]
    [Category("Wilsk.Component.WaypointTracker Tests")]
    internal class WaypointTrackerComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {
        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _receivedMessages.Clear();
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetNewWaypoint);
        }

        [Test]
        public void NoMessagesSentIfWaypointListEmptTest()
        {
            var wpt = new WaypointTrackerComponent(_tmpl, _bus);
            wpt.Update();
            Assert.AreEqual(0, _receivedMessages.Count); // no message sent

            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, new Vector3(100, 100));
            wpt.Update();
            Assert.AreEqual(0, _receivedMessages.Count); // no message sent
        }

        [Test]
        public void SendsNewWaypointMessageWhenWaypointsUpdatedTest()
        {
            var wpt = new WaypointTrackerComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, Vector3.zero);
            _bus.SendMessage(this, MessageBusMessageType.ComponentWaypointsUpdated, new List<Vector3>()
            {
                new Vector3(100, 100),
                new Vector3(200, 200)
            });

            wpt.Update();

            AssertHasSingleMessageWithPayload(MessageBusMessageType.ComponentSetNewWaypoint, new Vector3(100, 100));
        }

        [Test]
        public void AutocompletesWaypointIfNearStartingPointAndRaisesOnlyOneMessageTest()
        {
            var wpt = new WaypointTrackerComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, new Vector3(100, 100));
            _bus.SendMessage(this, MessageBusMessageType.ComponentWaypointsUpdated, new List<Vector3>()
            {
                new Vector3(100, 100),
                new Vector3(200, 200)
            });

            wpt.Update();

            AssertHasSingleMessageWithPayload(MessageBusMessageType.ComponentSetNewWaypoint, new Vector3(200, 200));}

        [Test]
        public void CompletesWaypointIfNearbyTest()
        {
            var wpt = new WaypointTrackerComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, Vector3.zero);
            _bus.SendMessage(this, MessageBusMessageType.ComponentWaypointsUpdated, new List<Vector3>()
            {
                new Vector3(100, 100),
                new Vector3(200, 200)
            });

            wpt.Update();

            AssertHasSingleMessageWithPayload(MessageBusMessageType.ComponentSetNewWaypoint, new Vector3(100, 100));
            _receivedMessages.Clear();
            
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, new Vector3(100, 100));
            wpt.Update();

            AssertHasSingleMessageWithPayload(MessageBusMessageType.ComponentSetNewWaypoint, new Vector3(200, 200));
        }

        [Test]
        public void NoMessageSentOnReachingLastWaypointTest()
        {
            var wpt = new WaypointTrackerComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, new Vector3(100, 100));
            _bus.SendMessage(this, MessageBusMessageType.ComponentWaypointsUpdated, new List<Vector3>()
            {
                new Vector3(100, 100)
            });

            wpt.Update();

            Assert.AreEqual(0, _receivedMessages.Count);
        }
    }
}
