﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    [TestFixture]
    [Category("Wilsk.Component.MoveToWaypoint Tests")]
    internal class MoveToWaypointComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {
        private MoveToWaypointComponent _mtw;

#pragma warning disable 0414
        private StatsComponent _sc;
#pragma warning restore 0414

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() {  MovementSpeed = 5, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _mtw = new MoveToWaypointComponent(_tmpl, _bus);
            _sc = new StatsComponent(_tmpl, _bus);

            _bus.SendMessage(this, MessageBusMessageType.ComponentSetInitialPosition, Vector3.zero);

            _receivedMessages.Clear();
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
        }
        
        [Test]
        public void UpdatesPositionIfWaypointIsDifferentToPositionTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewWaypoint, new Vector3(100, 100));

            _mtw.Update();
            AssertHasSingleMessageWithPayloadNotEqualTo(MessageBusMessageType.ComponentPositionUpdated, Vector3.zero);
        }

        [Test]
        public void DoesntUpdatePositionWithMessageIfAtWaypointAlreadyTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewWaypoint, Vector3.zero);

            _mtw.Update();
            Assert.AreEqual(0, _receivedMessages.Count);
        }
    }
}
