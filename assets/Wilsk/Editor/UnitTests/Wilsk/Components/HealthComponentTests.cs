﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using Core.Messaging;
    using Components;
    using Entities;

    [TestFixture]
    [Category("Wilsk.Entity.Health Tests")]
    internal class HealthComponentTests
    {
        private ComponentMessageBus _bus = new ComponentMessageBus();

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() {  StartingHealth = 100 }
        };

        private EntitySettings.EnemyTemplate _tmpl2 = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { StartingHealth = 97 }
        };

        [Test]
        public void ShouldStartWith100HealthTest()
        {
            var h = new HealthComponent(_tmpl, _bus);

            Assert.AreEqual(100f, h.CurrentHealth);
            Assert.AreEqual(100f, h.MaxHealth);
        }

        [Test]
        public void StartingHealthShouldSetCurrentAndMaxTest()
        {
            var h = new HealthComponent(_tmpl2, _bus);
            
            Assert.AreEqual(97f, h.CurrentHealth);
            Assert.AreEqual(97f, h.MaxHealth);
        }

        [Test]
        public void ShouldReduceHealthOnTakeDamageTest()
        {
            var h = new HealthComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentDamageReceived, 5f);
            
            Assert.AreEqual(95f, h.CurrentHealth);
            Assert.AreEqual(100f, h.MaxHealth);
        }

        [Test]
        public void ShouldLimitHealthToZeroTest()
        {
            var h = new HealthComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentDamageReceived, 105f);

            Assert.AreEqual(0f, h.CurrentHealth);
            Assert.AreEqual(100f, h.MaxHealth);
        }
        

        [Datapoint]
        public float zero = 0;

        [Datapoint]
        public float fifty = 50;

        [Datapoint]
        public float seventyfive = 75; 

        [Datapoint]
        public float onehundred = 100;

        [Theory]
        public void PercentHealthShouldRepresentPercentageHealthTest(float num)
        {
            Assume.That(num >= 0 && num <= 100);

            var h = new HealthComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentDamageReceived, num);

            Assert.AreEqual(100 - num, h.PercentHealth);            
        }
    }
}
