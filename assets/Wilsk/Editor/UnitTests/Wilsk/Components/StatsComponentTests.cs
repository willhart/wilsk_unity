﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;

    using Core.Messaging;
    using Components;
    using Entities;

    [TestFixture]
    [Category("Wilsk.Components.Stats Tests")]
    internal class StatsComponentTests
    {
        private ComponentMessageBus _bus = new ComponentMessageBus();

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };
        
        [Test]
        public void ShouldGetStatsFromEntitySettingsTest()
        {
            var s = new StatsComponent(_tmpl, _bus);

            Assert.AreEqual(1, s.Stats.Damage); // s1 updated?
            Assert.AreEqual(1, s.Stats.MovementSpeed);
            Assert.AreEqual(1, s.Stats.Stealth);
            Assert.AreEqual(100, s.Stats.StartingHealth);
        }

        [Test]
        public void ShouldMergeSafelyTest()
        {
            var s = new StatsComponent(_tmpl, _bus);
            var s2 = new EntityStats() { Damage = 2, MovementSpeed = 3, Stealth = 4 };

            s.Merge(s2);

            Assert.AreEqual(3, s.Stats.Damage); // s1 updated?
            Assert.AreEqual(4, s.Stats.MovementSpeed); 
            Assert.AreEqual(5, s.Stats.Stealth);

            Assert.AreEqual(2, s2.Damage); // s2 unchanged?
            Assert.AreEqual(3, s2.MovementSpeed);
            Assert.AreEqual(4, s2.Stealth);
        }

    }
}
