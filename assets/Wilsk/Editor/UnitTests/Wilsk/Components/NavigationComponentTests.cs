﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using UnityEngine;

    using Core.Messaging;
    using Entities;
    using Graph;

    [TestFixture]
    [Category("Wilsk.Component.NavigationComponent Tests")]
    internal class NavigationComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {
#pragma warning disable 0414
        private NavigationComponent _nc;
#pragma warning restore 0414

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _receivedMessages.Clear();

            _nc = new NavigationComponent(_tmpl, _bus);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, Vector3.zero);
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.PathRequested);
        }
        
        [Test]
        public void SettingNewDestinationTriggersPathRequestTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewDestination, new Vector3(100, 100));

            AssertHasSingleMessageWithPayload(MessageBusMessageType.PathRequested, new PathfindingRequestArgs()
            {
                Origin = Vector3.zero,
                Destination = new Vector3(100, 100),
                Graph = null,
                Requester = _bus
            });
        }
    }
}
