﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using System.Collections.Generic;

    using Core.Messaging;
    using Entities;
    using Input.Tests;
    using Input;
    using System.Linq;

    [TestFixture]
    [Category("Wilsk.Component.WaypointTracker Tests")]
    internal class TopDown2DUnitInputComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _receivedMessages.Clear();
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentOnRightClick);
        }

        [Test]
        public void RaisesNoMessagesIfButtonNotClickedTest()
        {
            var ip = new TestInputProxy();
            ip.DigitalResult = false;

            var inputComponent = new TopDown2DUnitInputComponent(_tmpl, _bus, ip);
            inputComponent.Update();

            AssertNoMessages();
        }

        [Test]
        public void RasesMessagesIfRightButtonClickedShiftSetToFalseIfNotHeldTest()
        {
            var digiQueue = new Queue<bool>();
            digiQueue.Enqueue(false);

            var ip = new TestInputProxy(digiQueue, null);

            var inputComponent = new TopDown2DUnitInputComponent(_tmpl, _bus, ip);
            inputComponent.Update();

            Assert.IsFalse(_receivedMessages.First().Value.Get<InputEventArgs>().UseModifier);
        }

        [Test]
        public void RasesMessagesIfRightButtonClickedShiftSetToTrueIfHeldTest()
        {
            var ip = new TestInputProxy();

            var inputComponent = new TopDown2DUnitInputComponent(_tmpl, _bus, ip);
            inputComponent.Update();

            Assert.IsTrue(_receivedMessages.First().Value.Get<InputEventArgs>().UseModifier);
        }
    }
}
