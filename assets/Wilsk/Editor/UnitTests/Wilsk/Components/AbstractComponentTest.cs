﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    using Core.Messaging;

    internal abstract class AbstractComponentTest : IMessageBusSubscriber
    {
        protected ComponentMessageBus _bus = new ComponentMessageBus();

        protected readonly Dictionary<MessageBusMessageType, MessageBusEventArgs> _receivedMessages =
            new Dictionary<MessageBusMessageType, MessageBusEventArgs>();

        protected virtual void FixtureInit()
        {
            Subscribe();
        }

        public abstract void Subscribe();

        public virtual void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            // throw if adding duplicate
            _receivedMessages.Add(type, args);
        }

        public void AssertHasSingleMessageWithPayload<T>(MessageBusMessageType type, T payload)
        {
            Assert.AreEqual(1, _receivedMessages.Count);
            Assert.AreEqual(type, _receivedMessages.Keys.First());
            Assert.AreEqual(payload, _receivedMessages.Values.First().Get<T>());
        }

        public void AssertHasSingleMessageWithPayloadNotEqualTo<T>(MessageBusMessageType type, T payload)
        {
            Assert.AreEqual(1, _receivedMessages.Count);
            Assert.AreEqual(type, _receivedMessages.Keys.First());
            Assert.AreNotEqual(payload, _receivedMessages.Values.First().Get<T>());
        }

        public void AssertNoMessages()
        {
            Assert.AreEqual(0, _receivedMessages.Count);
        }
    }
}
