﻿namespace Wilsk.Components.Tests
{
    using NUnit.Framework;

    using Core.Messaging;
    using Entities;

    [TestFixture]
    [Category("Wilsk.Component.RemoveOneDeathComponent Tests")]
    internal class RemoveOnDeathComponentTests : AbstractComponentTest, IMessageBusSubscriber
    {
#pragma warning disable 0414
        private RemoveOnDeathComponent _rod;
#pragma warning restore 0414

        private EntitySettings.EnemyTemplate _tmpl = new EntitySettings.EnemyTemplate()
        {
            Stats = new EntityStats() { Damage = 1, MovementSpeed = 1, Stealth = 1, StartingHealth = 100 }
        };

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            FixtureInit();
        }

        [SetUp]
        public void SetupTest()
        {
            _receivedMessages.Clear();
            _rod = new RemoveOnDeathComponent(_tmpl, _bus);
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.DestroyGameObject);
        }
        
        [Test]
        public void TriggersRemoveMessageOnDeathTest()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentHealthDepleted, 0);
            AssertHasSingleMessageWithPayload(MessageBusMessageType.DestroyGameObject, 0);
        }
    }
}
