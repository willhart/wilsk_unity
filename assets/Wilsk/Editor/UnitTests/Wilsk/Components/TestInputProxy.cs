﻿namespace Wilsk.Input.Tests
{
    using System.Collections.Generic;
    using UnityEngine;

    using Input;

    /// <summary>
    /// A simple class for testing input
    /// </summary>
    public class TestInputProxy : IInputProxy
    {
        private readonly Queue<bool> _digitalQueue;
        private readonly Queue<float> _analogQueue;

        public TestInputProxy()
        {
            DigitalResult = true;
            AnalogResult = 1f;
        }

        public TestInputProxy(Queue<bool> digital, Queue<float> analog) : this()
        {
            _digitalQueue = digital;
            _analogQueue = analog;
        }

        public Vector3 PointerScreenLocation
        {
            get
            {
                return Vector3.zero;
            }
        }

        public float GetAnalog(InputControlTypes input)
        {
            if (_analogQueue != null && _analogQueue.Count > 0)
            {
                return _analogQueue.Dequeue();
            }

            return AnalogResult;
        }

        public bool GetDigital(InputControlTypes input)
        {
            if (_digitalQueue != null && _digitalQueue.Count > 0)
            {
                return _digitalQueue.Dequeue();
            }

            return DigitalResult;
        }

        public void Update() { }

        public bool DigitalResult { get; set; }
        public float AnalogResult { get; set; }
    }
}
