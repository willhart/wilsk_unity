﻿namespace Wilsk.Core.DataStructures.Tests
{
    using NUnit.Framework;
    using DataStructures;
    using System;

    [TestFixture]
    [Category("Wilsk.Core.DataStructures.MinHeap Tests")]
    internal class MinHeapTests
    {
        /// <summary>
        /// A quick class for testing the heap
        /// </summary>
        public class TestHeapItem : IHeapItem<TestHeapItem>
        {
            public TestHeapItem(int value = 0)
            {
                Value = value;
            }

            public int CompareTo(TestHeapItem other)
            {
                return -(Value.CompareTo(other.Value));
            }

            public int Value { get; set; }
            public int HeapIndex { get; set; }
        }

        [Test]
        public void AddingItemShouldIncreaseCountTest()
        {
            var h = new MinHeap<TestHeapItem>(1);

            Assert.AreEqual(0, h.Count);
            h.Add(new TestHeapItem());
            Assert.AreEqual(1, h.Count);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void AddingTooManyItemsShouldRaiseArgumentExceptionTest()
        {
            var h = new MinHeap<TestHeapItem>(1);

            h.Add(new TestHeapItem(-1));
            h.Add(new TestHeapItem(1));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PoppingWithEmptyHeapShouldRaiseExceptionTest()
        {
            var h = new MinHeap<TestHeapItem>(1);
            h.Pop();
        }

        [Test]
        public void PoppingShouldReturnAnItemTest()
        {
            var h = new MinHeap<TestHeapItem>(1);
            var hi = new TestHeapItem(4);

            h.Add(hi);

            Assert.AreSame(hi, h.Pop());
        }

        [Test]
        public void PoppingShouldReturnItemsBasedOnTheComparerTest()
        {
            var h = new MinHeap<TestHeapItem>(4);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);
            var h3 = new TestHeapItem(-1);
            var h4 = new TestHeapItem(0);

            h.Add(h1);
            h.Add(h2);
            h.Add(h3);
            h.Add(h4);

            // should be in heap     [-1,  0,  0,  1]
            // which corresponds to  [h3, h2, h4, h1]
            // 2 and 4 could be switched? 
            Assert.AreSame(h3, h.Pop());

            h.Pop(); // don't care what order these are in
            h.Pop();

            Assert.AreSame(h1, h.Pop());
        }

        [Test]
        public void CountShouldUpdateWhenItemsArePoppedTest()
        {
            var h = new MinHeap<TestHeapItem>(2);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);
            h.Add(h1);
            h.Add(h2);

            Assert.AreEqual(2, h.Count);
            h.Pop();
            Assert.AreEqual(1, h.Count);
            h.Pop();
            Assert.AreEqual(0, h.Count);
        }

        [Test]
        public void ContainsShouldReturnTrueForContainedItemTest()
        {
            var h = new MinHeap<TestHeapItem>(2);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);

            h.Add(h1);
            h.Add(h2);

            Assert.IsTrue(h.Contains(h1));
        }

        [Test]
        public void ContainsShouldReturnFalseForNotContainedItemTest()
        {
            var h = new MinHeap<TestHeapItem>(2);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);
            
            h.Add(h2);

            Assert.IsFalse(h.Contains(h1));
        }

        [Test]
        public void ReprioritiseItemShouldHaveNoEffectIfTreeSortedTest()
        {
            var h = new MinHeap<TestHeapItem>(2);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);

            h.Add(h1);
            h.Add(h2);

            h.ReprioritiseItem(h1);

            Assert.AreSame(h2, h.Pop());
            Assert.AreSame(h1, h.Pop());
        }

        [Test]
        public void ReprioritiseItemShouldResortTest()
        {
            var h = new MinHeap<TestHeapItem>(2);

            var h1 = new TestHeapItem(1);
            var h2 = new TestHeapItem(0);

            h.Add(h1);
            h.Add(h2);

            h1.Value = -100;
            h.ReprioritiseItem(h1);

            Assert.AreSame(h1, h.Pop());
            Assert.AreSame(h2, h.Pop());
        }

        [Test]
        public void SizeShouldReturnInternalLengthOfHeapTest()
        {
            var h = new MinHeap<TestHeapItem>(12);
            Assert.AreEqual(12, h.Size);
        }
    }
}
