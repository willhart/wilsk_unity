﻿namespace Wilsk
{
    using Zenject;

    using Core;
    using Core.Async;
    using Core.Logic;
    using Core.Messaging;
    using Core.Screens;
    using Entities;
    using Example.Logic;
    using Example.Screens;
    using Input;

    /// <summary>
    /// These dependencies are application wide and should remain static. Hence
    /// they are injected on the global composition root. It contains objects
    /// such as the global game state, levels and the messagebus for communicating
    /// between objects
    /// </summary>
    public class GlobalDependencyInstaller : MonoInstaller
    {
#pragma warning disable 0649
        /// <summary>
        /// Settings for the game to be injected into all classes which require it.
        /// </summary>
        public GameSettings _gameSettings;

        /// <summary>
        /// Describes the entities that make up a game
        /// </summary>
        public EntitySettings SceneSettings;
#pragma warning restore 0649

        public override void InstallBindings()
        {
            // bind the message bus
            Container.Bind<MessageBus>().ToSingle();

            // Set up dependencies for async jobs
            Container.Bind<AsynchronousJobQueue>().ToSingleGameObject("AsyncJobQueue");

            // Inject the chosen control schema
            Container.Bind<IInputProxy>().ToSingle<TopDown2dInputProxy>();

            // bind the screen factory for creating screens
            Container.Bind<ScreenFactory>().ToSingle();
            
            // bind levels and screens 
            Container.Bind<IMainMenu>().ToTransient<MainMenuScreen>();
            Container.Bind<ISplashScreen>().ToTransient<SplashScreen>(); // comment out for no splash screen
            Container.Bind<IScreen>().ToTransient<SimpleGameExampleMainScreen>(); // Example only - delete in your game and add your own

            // set up games
            Container.Bind<Game>().ToSingle();
            Container.Bind<GameSettings>().ToSingleInstance(_gameSettings);

            // global enemy factory and settings
            Container.BindGameObjectFactory<LocalEnemy.Factory>(SceneSettings.BaseEnemyPrefab);
            Container.Bind<EntitySettings>().ToSingleInstance(SceneSettings);
        }
    }
}