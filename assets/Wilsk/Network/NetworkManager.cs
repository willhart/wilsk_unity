﻿namespace Wilsk.Network
{
    using System;
    using UnityEngine;

    /// <summary>
    /// A simple class for handling network connections
    /// </summary>
    public class NetworkManager
    {
        #region Events
        /// <summary>
        /// Triggered when a lobby is joined
        /// </summary>
        public event EventHandler JoinedLobby;

        /// <summary>
        /// Triggered when a room is joined
        /// </summary>
        public event EventHandler JoinedRoom;
        #endregion

        #region Constants
        /// <summary>
        /// The name of the room to join
        /// </summary>
        private const string RoomName = "MainLobby";
        #endregion

        #region Unity Methods
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="offline"></param>
        public NetworkManager(bool offline)
        {
            Offline = offline;

            if (Offline)
            {
                PhotonNetwork.offlineMode = true;
                Debug.LogWarning("Offline mode not yet fully supported");
                OnJoinedLobby();
                return;
            }

            Connect();
        }
        #endregion

        #region Photon Handler Methods
        /// <summary>
        /// Handles joining a lobby - automatically creates a room
        /// </summary>
        public void OnJoinedLobby()
        {
            if (JoinedLobby != null)
            {
                JoinedLobby(this, new EventArgs());
            }

            PhotonNetwork.JoinOrCreateRoom(RoomName, new RoomOptions()
            {
                maxPlayers = 4,
                isOpen = true,
                isVisible = true
            }, TypedLobby.Default);
        }

        /// <summary>
        /// Handles an error in joining a room
        /// </summary>
        public void OnPhotonJoinFailed()
        {
            Debug.LogError("Failed to join random room!");
        }

        /// <summary>
        /// Handles joining a room
        /// </summary>
        public void OnJoinedRoom()
        {
            if (JoinedRoom != null)
            {
                JoinedRoom(this, new EventArgs());
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Connects using the given settings
        /// </summary>
        private void Connect()
        {
            PhotonNetwork.ConnectUsingSettings("0.1.0");
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets a value indicating whether the network manager is in offline mode
        /// </summary>
        public bool Offline { get; private set; }

        /// <summary>
        /// Gets a value indicating the number of connected players
        /// </summary>
        public int ConnectedPlayers { get; private set; }
        #endregion
    }
}
