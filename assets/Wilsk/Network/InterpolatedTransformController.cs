﻿namespace Wilsk.Network
{
    using UnityEngine;

    /// <summary>
    /// A simple class for taking position and rotation and interpolating desired position and rotation over time
    /// </summary>
    public class InterpolatedTransform
    {
        #region Private Members
        /// <summary>
        /// The last time sync information was received over the network
        /// </summary>
        private float lastSynchronisationTime = Time.time;

        /// <summary>
        /// The last time that the game state was updated
        /// </summary>
        private float syncTime = 0f;

        /// <summary>
        /// The amount of time between now and the last sync
        /// </summary>
        private float syncDelay = 0f;

        /// <summary>
        /// The position the player was at the last sync
        /// </summary>
        private Vector3 lastSyncPosition = Vector3.zero;

        /// <summary>
        /// The position we would like to place the object
        /// </summary>
        private Vector3 desiredPosition = Vector3.zero;

        /// <summary>
        /// The rotation the player was at the last sync
        /// </summary>
        private Quaternion lastSyncRotation = Quaternion.identity;

        /// <summary>
        /// The rotation we would like to for the object
        /// </summary>
        private Quaternion desiredRotation = Quaternion.identity;
        #endregion
        
        #region Public Methods
        /// <summary>
        /// Updates the desired position and rotation of the transform
        /// </summary>
        /// <param name="nextPos"></param>
        /// <param name="nextRot"></param>
        public void Set(Vector3 currentPos, Quaternion currentRot, Vector3 nextPos, Vector3 velocity, Quaternion nextRot)
        {
            syncTime = 0f;
            syncDelay = Time.time - lastSynchronisationTime;
            lastSynchronisationTime = Time.time;

            desiredPosition = nextPos + syncDelay * velocity;
            desiredRotation = nextRot;

            lastSyncPosition = currentPos;
            lastSyncRotation = currentRot;
        }

        /// <summary>
        /// Updates the position and rotation of the object based on the elapsed time
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(float deltaTime)
        {
            syncTime += deltaTime;

            var lerpProportion = Mathf.Clamp01(syncTime / syncDelay);
            Position = Vector3.Lerp(lastSyncPosition, desiredPosition, lerpProportion);
            Rotation = Quaternion.Lerp(lastSyncRotation, desiredRotation, lerpProportion);
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the interpolated position of the transform
        /// </summary>
        public Vector3 Position { get; private set; }

        /// <summary>
        /// Gets the interpolated rotation of the transform
        /// </summary>
        public Quaternion Rotation { get; private set; }
        #endregion
    }
}