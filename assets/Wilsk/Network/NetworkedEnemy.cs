﻿namespace Wilsk.Network
{
    using System;
    using UnityEngine;

    using Core.Messaging;
    using Entities;
    using Components;
    
    /// <summary>
    /// A class to use for enemies that must be instantiated over the network.
    /// Performs manual injection as in this contect Zenject is not available. If 
    /// the instantiation is not happening over the network, LocalEnemy can be used instead
    /// </summary>
    [RequireComponent(typeof(EnemyInteractionComponent))]
    public class NetworkedEnemy : Photon.MonoBehaviour
    {
        #region Private Members
        /// <summary>
        /// Holds enemy logic
        /// </summary>
        private EnemyComponentBundle _enemy;

        /// <summary>
        /// A static factory for components to be used in all spawned instances
        /// </summary>
        private static readonly ComponentFactory _factory = new ComponentFactory();

        /// <summary>
        /// A  reference to the global message bus
        /// </summary>
        private static readonly MessageBus _globalBus;
        #endregion

        #region Unity Methods
        /// <summary>
        /// TODO Inject enemy template via Photon instantiation args
        /// TODO Provide access to the global message bus
        /// </summary>
        void Awake()
        {
            // setup the enemy and perform manual injection of required classes
            _enemy = new EnemyComponentBundle(_factory, new ComponentMessageBus(), _globalBus);
        }

        void SetupEnemy(EntitySettings.EnemyTemplate template)
        {
            _enemy.SetupEnemy(template, _globalBus);
            GetComponent<EnemyInteractionComponent>().Init(_enemy, template);
        }
        
        void Update()
        {
            _enemy.Update();
        }
        #endregion
    }
}
