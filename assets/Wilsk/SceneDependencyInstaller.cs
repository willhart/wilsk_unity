﻿#define REQUIRE_WILSK_GRAPH

namespace Wilsk
{
    using Zenject;

    using Components;
    using Core.Logic;
    using Core.Messaging;
    using Entities;
    using Example.Logic;
    using Graph;

    public class SceneDependencyInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameLogic>().ToTransient<SimpleExampleGameLogic>(); // replace with your desired game logic
            Container.Bind<VisibilityManager>().ToSingle();

            // Set up dependencies for path finding
            Container.Bind<Graph.GraphBuilderFactory>().ToSingle();
            Container.Bind<Graph.MeshGraphVisualiser>().ToSingle();
            Container.Bind<Graph.Graph>().ToSingle();

            // bind components
            Container.Bind<EnemyComponentBundle>().ToTransient();
            Container.Bind<ComponentFactory>().ToSingle();
            Container.Bind<ComponentMessageBus>().ToTransient();
            Container.Bind<IPathFinder>().ToSingle<AStarPathFinder>();
        }
    }
}