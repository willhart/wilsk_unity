﻿namespace Wilsk.Components
{
    using System;
    using System.Collections.Generic;

    using Core.Messaging;
    using Entities;

    public abstract class AbstractComponentBundle : IMessageBusSubscriber
    {
        /// <summary>
        /// A dictionary of loaded components
        /// </summary>
        protected readonly Dictionary<ComponentType, IComponent> _loadedComponents =
            new Dictionary<ComponentType, IComponent>();

        /// <summary>
        /// The internal message bus to use
        /// </summary>
        protected ComponentMessageBus _bus;

        /// <summary>
        /// The global message bus to use
        /// </summary>
        protected MessageBus _globalBus;

        /// <summary>
        /// The factory for creating components
        /// </summary>
        protected ComponentFactory _factory;

        /// <summary>
        /// Default constructor
        /// </summary>
        public AbstractComponentBundle(ComponentFactory factory, ComponentMessageBus componentBus, MessageBus globalBus)
        {
            _bus = componentBus;
            _globalBus = globalBus;
            _factory = factory;

            Subscribe();
        }

        /// <summary>
        /// Builds up a network of components based on the passed flags
        /// </summary>
        /// <param name="template"></param>
        public void BuildComponents(EntitySettings.EnemyTemplate template)
        {
            foreach (var component in GetFlags(template.Components))
            {
                _loadedComponents.Add(component, _factory.Create(template, component, _bus));
            }
        }

        /// <summary>
        /// Gets the given type of component from the bundle or returns null if the 
        /// component is not in the bundle
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public IComponent GetComponent(ComponentType component)
        {
            if (!_loadedComponents.ContainsKey(component))
            {
                return null;
            }

            return _loadedComponents[component];
        }

        /// <summary>
        /// Get the flags that are in the give ComponentType flag
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected static IEnumerable<ComponentType> GetFlags(ComponentType input)
        {
            foreach (ComponentType value in Enum.GetValues(typeof(ComponentType)))
            {
                if ((value & input) == value)
                {
                    yield return value;
                }
            }
        }

        public virtual void Subscribe()
        {
            //
        }

        public virtual void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            //
        }

        /// <summary>
        /// Gets an IEnumerable of installed components
        /// </summary>
        public IEnumerable<IComponent> Components
        {
            get
            {
                return new List<IComponent>(_loadedComponents.Values);
            }
        }
    }
}
