﻿namespace Wilsk.Components
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    using Core.Messaging;
    using Graph;
    using Input;
    using Entities;

    public class ComponentFactory
    {
#pragma warning disable 0649
        [Inject]
        private IPathFinder _pathfinder;

        [Inject]
        private IInputProxy _input;
#pragma warning restore 0649

        private static readonly Dictionary<ComponentType, Type> _types = new Dictionary<ComponentType, Type>()
        {
            { ComponentType.Health, typeof(HealthComponent) },
            { ComponentType.Stats, typeof(StatsComponent) },
            { ComponentType.RemoveOnDeath, typeof(RemoveOnDeathComponent) },
            { ComponentType.Navigation, typeof(NavigationComponent) },
            { ComponentType.WaypointTracker, typeof(WaypointTrackerComponent) },
            { ComponentType.MoveToWaypoint, typeof(MoveToWaypointComponent) },
            { ComponentType.Selectable, typeof(SelectableComponent) },
            { ComponentType.TopDown2DInput, typeof(TopDown2DUnitInputComponent) },
            { ComponentType.Visible, typeof(VisibilityComponent) },
            { ComponentType.SpriteRenderer, typeof(Sprite2DRendererComponent) }

        };

        public IComponent Create(EntitySettings.EnemyTemplate template, ComponentType component, ComponentMessageBus bus)
        {
            if (_types.ContainsKey(component))
            {
                var type = _types[component];
                object[] args;

                // handle different constructors
                if (component == ComponentType.TopDown2DInput)
                {
                    args = new object[] { template, bus, _input };
                }
                else
                {
                    // handle standard constructors
                    args = new object[] { template, bus };
                }

                // return other instances with standard constructors
                return (IComponent)Activator.CreateInstance(type, args);
            }

            Debug.LogError("Unknown Component Type " + component.ToString() + " in ComponentFactory. Returning null");
            return null;
        }
    }
}
