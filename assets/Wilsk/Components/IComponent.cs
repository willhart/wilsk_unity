﻿namespace Wilsk.Components
{
    public interface IComponent
    {
        void Update();
    }
}
