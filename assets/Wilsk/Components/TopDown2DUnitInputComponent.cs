﻿namespace Wilsk.Components
{
    using Core.Messaging;
    using Entities;
    using Input;

    public class TopDown2DUnitInputComponent : AbstractComponent
    {
        /// <summary>
        /// Handles game input
        /// </summary>
        private IInputProxy _input;

        public TopDown2DUnitInputComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus, IInputProxy input) : base(template, bus)
        {
            _input = input;
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args) { }
        public override void Subscribe() { }

        public override void Update()
        {
            var shiftHeld = _input.GetDigital(InputControlTypes.LeftShift);

            if (_input.GetDigital(InputControlTypes.RightClick))
            {
                _bus.SendMessage(this, MessageBusMessageType.ComponentOnRightClick, new InputEventArgs()
                {
                    UseModifier = shiftHeld,
                    Location = _input.PointerScreenLocation
                });
            }
        }
    }
}
