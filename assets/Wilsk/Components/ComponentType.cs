﻿namespace Wilsk.Components
{
    using System;

    [Flags]
    public enum ComponentType
    {
        Stats             = 1 << 0,
        Health            = 1 << 1,
        RemoveOnDeath     = 1 << 2,
        Selectable        = 1 << 3,
        Visible           = 1 << 4,

        Navigation        = 1 << 5,
        WaypointTracker   = 1 << 6,
        MoveToWaypoint    = 1 << 7,

        TopDown2DInput    = 1 << 8,

        SpriteRenderer    = 1 << 9
    }
}
