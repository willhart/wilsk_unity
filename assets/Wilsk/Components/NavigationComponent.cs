﻿namespace Wilsk.Components
{
    using System;
    using UnityEngine;

    using Core.Messaging;
    using Entities;
    using Graph;

    public class NavigationComponent : AbstractComponent
    {
        /// <summary>
        /// The current position
        /// </summary>
        private Vector3 _currentPosition;

        public NavigationComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus) { }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            switch (type)
            {
                case MessageBusMessageType.ComponentSetNewDestination:
                    var prargs = new PathfindingRequestArgs() { Origin = _currentPosition, Destination = args.Get<Vector3>(), Requester = _bus };
                    _bus.SendMessage(this, MessageBusMessageType.PathRequested, prargs);
                    break;

                case MessageBusMessageType.ComponentPositionUpdated:
                case MessageBusMessageType.ComponentSetInitialPosition:
                    _currentPosition = args.Get<Vector3>();
                    break;

                default:
                    throw new ArgumentException("Unknown message type received by NagivationComponent");
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetNewDestination);
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetInitialPosition);
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
        }

        public override void Update() { }
    }
}
