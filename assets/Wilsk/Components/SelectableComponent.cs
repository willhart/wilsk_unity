﻿namespace Wilsk.Components
{
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    public class SelectableComponent : AbstractComponent
    {
        public SelectableComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus)
        {
            IsSelected = false;
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ComponentOnLeftClick)
            {
                IsSelected = !IsSelected;
                _bus.SendMessage(this, MessageBusMessageType.ComponentSetSelected, IsSelected);
            }
            else if (type == MessageBusMessageType.ComponentSetSelected && args.Sender != this)
            {
                IsSelected = args.Get<bool>();
            }
            else if (type != MessageBusMessageType.ComponentOnLeftClick &&
                type != MessageBusMessageType.ComponentSetSelected)
            {
                Debug.LogWarning("SelectableComponent received unexpected message type: " + type.ToString());
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentOnLeftClick);
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetSelected);
        }

        public override void Update()
        {
            //
        }

        public bool IsSelected { get; private set; }
    }
}
