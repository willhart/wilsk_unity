﻿namespace Wilsk.Components
{
    using System.Collections.Generic;
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    public class WaypointTrackerComponent : AbstractComponent
    {
        #region Private Members
        /// <summary>
        /// The distance considered "close" or "arrived at" a location
        /// </summary>
        private static readonly float DeltaDistance = 0.1f;

        /// <summary>
        /// The waypoints currently in the unit's path
        /// </summary>
        private Queue<Vector3> _waypoints;

        /// <summary>
        /// The current waypoint we are navigating to
        /// </summary>
        private Vector3 _currentWaypoint = Vector3.zero;

        /// <summary>
        /// The current position of the object
        /// </summary>
        private Vector3 _position = Vector3.zero;
        #endregion

        #region Constructors
        public WaypointTrackerComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus){ }
        #endregion

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if(type == MessageBusMessageType.ComponentWaypointsUpdated)
            {
                // store waypoints
                _waypoints = new Queue<Vector3>(args.Get<List<Vector3>>());

                // remove first waypoint as this is the bottom corner of the current node grid
                _waypoints.Dequeue();

                // reset the current waypoint to "unset"
                _currentWaypoint = Vector3.zero;
                _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewWaypoint, _currentWaypoint);
            }
            else if (type == MessageBusMessageType.ComponentPositionUpdated)
            {
                _position = args.Get<Vector3>();
            }
            else
            {
                Debug.LogWarning("WaypointTracker received unexpected message - " + type.ToString());
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentWaypointsUpdated);
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
        }

        public override void Update()
        {
            if (_waypoints == null || _waypoints.Count == 0)
            {
                return;
            }

            var tmp = GetNextWaypoint(_position, _currentWaypoint);
            if (tmp != _currentWaypoint && tmp != Vector3.zero)
            {
                _currentWaypoint = tmp;
                _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewWaypoint, _currentWaypoint);
            }
        }

        private Vector3 GetNextWaypoint(Vector3 position, Vector3 waypoint)
        {
            // check if our current waypoint is zero - in this context "unset"
            if (waypoint == Vector3.zero)
            {
                // check if we have waypoints
                if (_waypoints == null || _waypoints.Count == 0)
                {
                    // nope, return "unset"
                    return Vector3.zero;
                }
            }
            
            // check if we have reached our current waypoint
            // if we haven't just return the current waypoint
            else if (!IsNear(position, waypoint, DeltaDistance))
            {
                return waypoint;
            }
            
            // otherwise, we are searching for a new waypoint...
            // dequeue waypoints until we find one which isn't "near" to our position
            while (_waypoints.Count > 0)
            {
                waypoint = _waypoints.Dequeue();

                if (!IsNear(position, waypoint, DeltaDistance))
                {
                    return waypoint;
                }
            }

            // unable to find a far waypoint, we must have reached the end of a path
            return Vector3.zero;
        }

        private bool IsNear(Vector3 position, Vector3 target, float delta)
        {
            return Vector3.Distance(position, target) < delta;
        }
    }
}
