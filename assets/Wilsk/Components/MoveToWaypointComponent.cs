﻿namespace Wilsk.Components
{
    using UnityEngine;

    using Core;
    using Core.Messaging;
    using Entities;

    /// <summary>
    /// A component which Lerps the body towards the waypoint
    /// </summary>
    public class MoveToWaypointComponent : AbstractComponent
    {
        #region Private Members
        /// <summary>
        /// The current waypoint we are navigating to
        /// </summary>
        private Vector3 _currentWaypoint = Vector3.zero;

        /// <summary>
        /// The current position of the object
        /// </summary>
        private Vector3 _position = Vector3.zero;
        #endregion

        #region Constructors
        public MoveToWaypointComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus){ }
        #endregion

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if(type == MessageBusMessageType.ComponentSetNewWaypoint)
            {
                _currentWaypoint = args.Get<Vector3>();
            }
            else if (type == MessageBusMessageType.ComponentSetInitialPosition ||
                type == MessageBusMessageType.ComponentPositionUpdated)
            {
                _position = args.Get<Vector3>();
            }
            else
            {
                Debug.LogWarning("WaypointPositionComponent received unexpected message - " + type.ToString());
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetInitialPosition);
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetNewWaypoint);
        }

        public override void Update()
        {
            if (_currentWaypoint == Vector3.zero) return;

            _position = Vector3.MoveTowards(_position, _currentWaypoint, _stats.MovementSpeed * TimeKeeper.DeltaTime);
            _bus.SendMessage(this, MessageBusMessageType.ComponentPositionUpdated, _position);
        }
    }
}
