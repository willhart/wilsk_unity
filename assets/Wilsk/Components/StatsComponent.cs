﻿namespace Wilsk.Components
{
    using System;
    using Core.Messaging;
    using Entities;

    [Serializable]
    public class StatsComponent : AbstractComponent
    {
        
        public StatsComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus) { }
        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args) { }
        public override void Subscribe() { }
        public override void Update() { }

        public void Merge(EntityStats stats)
        {
            Stats.Merge(stats);
        }

        public EntityStats Stats
        {
            get
            {
                return _stats;
            }
        }
    }
}
