﻿namespace Wilsk.Components
{
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    public class Sprite2DRendererComponent : AbstractComponent
    {
        /// <summary>
        /// The enemy template
        /// </summary>
        private readonly EntitySettings.EnemyTemplate _template;

        /// <summary>
        /// The game object to attach the renderer to
        /// </summary>
        private GameObject _renderObj;

        /// <summary>
        /// The sprite renderer that was created
        /// </summary>
        private SpriteRenderer _sprite;
        
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="template"></param>
        /// <param name="bus"></param>
        public Sprite2DRendererComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus)
        {
            _template = template;
        }

        /// <summary>
        /// Handle messages
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.RendererRequested)
            {
                _renderObj = args.Get<GameObject>();

                if (_renderObj == null)
                {
                    Debug.LogWarning("No render object found for Sprite2DRendererComponent");
                    return;
                }

                _sprite = _renderObj.AddComponent<SpriteRenderer>() as SpriteRenderer;
                _sprite.sprite = _template.EnemySprite;

                _renderObj.AddComponent<BoxCollider2D>();
            }
            else if (type == MessageBusMessageType.ComponentSetSelected)
            {
                var selected = args.Get<bool>();
                _sprite.color = selected ? Color.yellow : Color.white;
            }
        }

        /// <summary>
        /// Subscribe to messages
        /// </summary>
        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.RendererRequested);
            _bus.Subscribe(this, MessageBusMessageType.ComponentSetSelected);
        }

        public override void Update() { }
    }
}
