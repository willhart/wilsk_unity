﻿namespace Wilsk.Components
{
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    /// <summary>
    /// A simple class for controlling the health of objects
    /// </summary>
    public class HealthComponent : AbstractComponent
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public HealthComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus)
        {
            SetStartingHealth(template.Stats.StartingHealth);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Takes the given amount of damage to current health, ensuring current health is at least 0.
        /// If the health is reduced to zero then triggers the HealthDepleted event
        /// </summary>
        /// <param name="damage"></param>
        public void TakeDamage(float damage)
        {
            if (CurrentHealth == 0) return;

            CurrentHealth = Mathf.Max(0, CurrentHealth - damage);

            if (CurrentHealth <= 0)
            {
                _bus.SendMessage(this, MessageBusMessageType.ComponentHealthDepleted, 0);
            } 
            else
            {
                _bus.SendMessage(this, MessageBusMessageType.ComponentHealthUpdated, CurrentHealth);
            }
        }

        /// <summary>
        /// Sets the max and current health to the given value
        /// </summary>
        /// <param name="startingHealth"></param>
        public void SetStartingHealth(float startingHealth)
        {
            MaxHealth = startingHealth;
            CurrentHealth = startingHealth;
            _bus.SendMessage(this, MessageBusMessageType.ComponentHealthUpdated, CurrentHealth);
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            switch (type)
            {
                case (MessageBusMessageType.ComponentDamageReceived):
                    TakeDamage(args.Get<float>());
                    break;

                default:
                    Debug.LogError("Unknown message type received by HealthComponent");
                    break;
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentDamageReceived);
        }

        public override void Update()
        {
            //
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the maximum health of the entity
        /// </summary>
        public float MaxHealth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the current health of the entity
        /// </summary>
        public float CurrentHealth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value representing the percentage of full health the entity has
        /// </summary>
        public int PercentHealth
        {
            get
            {
                return (int)Mathf.Clamp(100 * CurrentHealth / MaxHealth, 0, 100);
            }
        }
        #endregion
    }
}
