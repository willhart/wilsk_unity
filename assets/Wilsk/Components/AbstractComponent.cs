﻿namespace Wilsk.Components
{
    using Core.Messaging;
    using Entities;

    public abstract class AbstractComponent : IMessageBusSubscriber, IComponent
    {
        /// <summary>
        /// The linked entities stats
        /// </summary>
        protected EntityStats _stats;

        /// <summary>
        /// The message bus
        /// </summary>
        protected ComponentMessageBus _bus;

        public AbstractComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus)
        {
            _bus = bus;
            _stats = template.Stats;

            Subscribe();
        }

        public abstract void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args);

        public abstract void Subscribe();

        public abstract void Update();
    }
}
