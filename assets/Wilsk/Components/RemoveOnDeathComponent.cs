﻿namespace Wilsk.Components
{
    using Core.Messaging;
    using Entities;
    
    public class RemoveOnDeathComponent : AbstractComponent
    {
        public RemoveOnDeathComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) : base(template, bus) { }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ComponentHealthDepleted)
            {
                _bus.SendMessage(this, MessageBusMessageType.DestroyGameObject, 0);
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentHealthDepleted);
        }

        public override void Update()
        {
            //
        }
    }
}
