﻿namespace Wilsk.Components
{
    using System.Collections.Generic;
    using UnityEngine;

    using Core.Messaging;
    using Entities;

    /// <summary>
    /// Handles an object being in line of sight of other components
    /// </summary>
    public class VisibilityComponent : AbstractComponent
    {
        private readonly HashSet<int> _visibleBy = new HashSet<int>();
        
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="template"></param>
        /// <param name="bus"></param>
        public VisibilityComponent(EntitySettings.EnemyTemplate template, ComponentMessageBus bus) 
            : base(template, bus)
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentRegisterForVisibility, null);
            Team = template.Team;

            _visibleBy.Add(template.Team);
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ComponentPositionUpdated)
            {
                Position = args.Get<Vector3>();
            }
        }

        public override void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
        }
        
        public override void Update() { }

        /// <summary>
        /// Returns true if the component is visible to the given team. 
        /// Always returns true for the components own team
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns></returns>
        public bool IsVisibleBy(int teamId)
        {
            return _visibleBy.Contains(teamId);
        }

        /// <summary>
        /// Sets the visibility of a given team
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="isVisible"></param>
        public void SetVisibleTo(int teamId, bool isVisible)
        {
            if (!isVisible)
            {
                _visibleBy.Remove(teamId);
            }
            else
            {
                _visibleBy.Add(teamId);
            }
        }

        /// <summary>
        /// Gets the team that this component belongs to
        /// </summary>
        public int Team { get; private set; }

        /// <summary>
        /// Gets the current position of the component
        /// </summary>
        public Vector3 Position { get; private set; }

        /// <summary>
        /// Gets the sight range of the component
        /// </summary>
        public float SightRange
        {
            get
            {
                return _stats.SightRange;
            }
        }
    }
}
