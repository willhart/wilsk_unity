﻿namespace Wilsk.Core
{
    using System;

    /// <summary>
    /// Specifies game settings in a serializable and injectible format
    /// </summary>
    [Serializable]
    public class GameSettings
    {
    }
}
