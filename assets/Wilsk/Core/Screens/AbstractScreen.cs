﻿namespace Wilsk.Core.Screens
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Messaging;
    using Zenject;

    /// <summary>
    /// An inheritable base class for all levels
    /// </summary>
    public abstract class AbstractScreen : IScreen
    {
        /// <summary>
        /// The game that the screen belongs to
        /// </summary>
        protected Game _game;

        /// <summary>
        /// A list of game objects managed by this class that are not destroyed by themselves
        /// but should be unloaded when the screen is unloaded. This includes things such as 
        /// Pause menus etc
        /// </summary>
        protected readonly List<GameObject> _managedObjects = new List<GameObject>();

        /// <summary>
        /// The message bus for sending game messages
        /// </summary>
        [Inject]
        private MessageBus _bus;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="game"></param>
        public AbstractScreen(Game game)
        {
            _game = game;
        }

        /// <summary>
        /// Configures the screen using the given parameters
        /// </summary>
        /// <param name="levelData"></param>
        public virtual void Configure(LevelConfiguration levelData)
        {
            // by default, do nothing
        }

        /// <summary>
        /// Updates the screen
        /// </summary>
        public virtual void Update() { }
        
        /// <summary>
        /// Loads the level asynchronously
        /// </summary>
        public virtual IEnumerator Load()
        {
            AsyncOperation async;

            if (LoadCumulatively)
            {
                async = Application.LoadLevelAdditiveAsync(SceneName);
            }
            else
            {
                async = Application.LoadLevelAsync(SceneName);
            }
            
            yield return async;

            Debug.Log("Loading complete");
        }
        
        /// <summary>
        /// Unloads the scene
        /// </summary>
        public void Unload()
        {
            _bus.SendMessage(this, MessageBusMessageType.ScreenUnloading, (int)ScreenType);
        }

        /// <summary>
        /// Gets a value which is true if the level has completed loading
        /// </summary>
        public virtual bool IsLoaded
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value which is true if the level should be loaded on top of other
        /// levels - such as a pause screen
        /// </summary>
        public virtual bool LoadCumulatively
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a list of managed objects for this screen that should be destroyed on unload
        /// </summary>
        public List<GameObject> ManagedObjects
        {
            get
            {
                return _managedObjects;
            }
        }

        /// <summary>
        /// Gets a unique string which identifies the level. Will be validated as
        /// unique at run time.
        /// </summary>
        public abstract ScreenType ScreenType { get; }

        /// <summary>
        /// Gets the scene name for loading in the level
        /// </summary>
        protected abstract string SceneName { get; }
    }
}
