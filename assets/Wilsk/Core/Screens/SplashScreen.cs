﻿namespace Wilsk.Core.Screens
{
    using System;
    using Zenject;

    /// <summary>
    /// Holds a splash screen which is displayed at level load for a set period of time
    /// </summary>
    public class SplashScreen : AbstractScreen, ISplashScreen
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="game"></param>
        public SplashScreen(Game game) : base(game) { }

        /// <summary>
        /// Gets an IScreen reference from this splashscreen
        /// </summary>
        /// <returns></returns>
        public IScreen GetScreen()
        {
            return this as IScreen;
        }

        /// <summary>
        /// Gets a value which is a unique name for the screen
        /// </summary>
        public override ScreenType ScreenType
        {
            get
            {
                return ScreenType.SplashScreen;
            }
        }

        /// <summary>
        /// Gets a value which is true when the splash screen has finished displaying
        /// </summary>
        public bool IsDone
        {
            get; private set;
        }

        /// <summary>
        /// Returns the name of a scene to load
        /// </summary>
        protected override string SceneName
        {
            get
            {
                return "SplashScreenScene";
            }
        }
    }
}
