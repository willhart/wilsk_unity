﻿namespace Wilsk.Core.Screens
{
    public interface ISplashScreen
    {
        IScreen GetScreen();
        bool IsDone { get; }
    }
}
