﻿namespace Wilsk.Core.Screens
{
    using System;
    using Zenject;

    using Example.Screens;

    public class ScreenFactory
    {
        private DiContainer _container;
     
        public ScreenFactory(DiContainer container)
        {
            _container = container;
        }

        public IScreen Create(ScreenType screenType)
        {
            switch (screenType)
            {
                case ScreenType.MainMenu:
                    return _container.Instantiate<MainMenuScreen>();

                case ScreenType.SplashScreen:
                    return _container.Instantiate<SplashScreen>();

                case ScreenType.GameLevel:
                    return _container.Instantiate<SimpleGameExampleMainScreen>();

                default:
                    throw new ArgumentException("Unknown screen type");
            }
        }
    }
}
