﻿namespace Wilsk.Core.Screens
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface IScreen
    {
        /// <summary>
        /// Returns true if the level should be loaded on top of existing levels
        /// i.e. such as a pause menu
        /// </summary>
        bool LoadCumulatively { get; }

        /// <summary>
        /// Loads the level asynchronously
        /// </summary>
        /// <returns></returns>
        IEnumerator Load();

        /// <summary>
        /// Unloads the screen
        /// </summary>
        void Unload();

        /// <summary>
        /// Updates the screen
        /// </summary>
        void Update();

        /// <summary>
        /// Sets configuration options for the level
        /// </summary>
        /// <param name="levelData"></param>
        void Configure(LevelConfiguration levelData);

        /// <summary>
        /// Gets a value indicating whether the level is fully loaded
        /// </summary>
        bool IsLoaded { get; }

        /// <summary>
        /// Gets an identifier for the level. 
        /// An interesting side note:
        /// http://programmers.stackexchange.com/questions/212638/is-it-poor-practice-to-name-a-property-member-the-same-as-the-declaring-type-in
        /// </summary>
        ScreenType ScreenType { get; }

        /// <summary>
        /// Gets a list of managed objects that should be destroyed to unload the level
        /// </summary>
        List<GameObject> ManagedObjects { get; }
    }
}
