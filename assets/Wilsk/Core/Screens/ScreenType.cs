﻿namespace Wilsk.Core.Screens
{
    /// <summary>
    /// An enum specifying the types of screens that are available.
    /// Modify as required with new screens
    /// </summary>
    public enum ScreenType
    {
        MainMenu = 0,
        SplashScreen = 1,
        GameLevel = 2
    }
}
