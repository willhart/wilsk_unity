﻿namespace Wilsk.Core.Screens
{
    using System;
    using Zenject;

    /// <summary>
    /// Holds a main menu
    /// </summary>
    public class MainMenuScreen : AbstractScreen, IMainMenu
    {
        /// <summary>
        /// The splash screen to show, or null if no splash screen is required
        /// </summary>
        private ISplashScreen _splashScreen;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="splashScreen"></param>
        public MainMenuScreen(Game game, [InjectOptional] ISplashScreen splashScreen) : base(game)
        {
            _splashScreen = splashScreen;
        }

        public bool HasSplash
        {
            get
            {
                return _splashScreen == null;
            }
        }

        /// <summary>
        /// Gets the level interface for the main menu
        /// </summary>
        /// <returns></returns>
        public IScreen GetScreen()
        {
            return this;
        }

        /// <summary>
        /// Gets the splash screen to show before the main menu
        /// </summary>
        /// <returns></returns>
        public ISplashScreen GetSplash()
        {
            return _splashScreen;
        }

        /// <summary>
        /// Gets a value which is a unique type for the scene
        /// </summary>
        public override ScreenType ScreenType
        {
            get
            {
                return ScreenType.MainMenu;
            }
        }

        /// <summary>
        /// Returns the name of a scene to load
        /// </summary>
        protected override string SceneName
        {
            get
            {
                return "MainMenuScene";
            }
        }
    }
}
