﻿namespace Wilsk.Core.Screens
{
    public interface IMainMenu
    {
        IScreen GetScreen();
        ISplashScreen GetSplash();
        bool HasSplash { get; }
    }
}
