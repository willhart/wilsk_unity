﻿namespace Wilsk.Core.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A class for handling de-coupled communication between game objects. It allows
    /// publication of events and subscription to those events
    /// </summary>
    public class MessageBus
    {
        /// <summary>
        /// Holds subscribers to particular events
        /// </summary>
        private readonly Dictionary<MessageBusMessageType, List<IMessageBusSubscriber>> _subscribers = 
            new Dictionary<MessageBusMessageType, List<IMessageBusSubscriber>>();
        
        /// <summary>
        /// Subscribes the given subscriber to the given message type
        /// </summary>
        /// <param name="subscriber"></param>
        /// <param name="message"></param>
        public void Subscribe(IMessageBusSubscriber subscriber, MessageBusMessageType message)
        {
            // add empty list of subscribers if required
            if (!_subscribers.ContainsKey(message)) 
            {
                _subscribers.Add(message, new List<IMessageBusSubscriber>());
            }

            // prevent resubscription
            if (!_subscribers[message].Contains(subscriber))
            {
                _subscribers[message].Add(subscriber);
            }
        }

        /// <summary>
        /// Unsubscribes the passed object from all messages
        /// </summary>
        /// <param name="subscriber"></param>
        public void Unsubscribe(IMessageBusSubscriber subscriber)
        {
            var keys = new List<MessageBusMessageType>(_subscribers.Keys);

            for (var i = 0; i < _subscribers.Count; ++i)
            {
                _subscribers[keys[i]].Remove(subscriber);
            }
        }

        /// <summary>
        /// Sends a string payload message of the given type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        /// <param name="payload"></param>
        public void SendMessage(object sender, MessageBusMessageType message, object payload)
        {
            // transmit message to all subscribers
            var args = new MessageBusEventArgs(message, sender, payload);
            SendMessage(message, args);
        }

        /// <summary>
        /// Generates up a string representation of the message bus connections
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("MESSAGE BUILDER SUBSCRIBERS");
            sb.Append(Environment.NewLine);
            sb.Append("------------------------------------");
            sb.Append(Environment.NewLine);

            foreach (var sub in _subscribers.Keys)
            {
                sb.Append(sub.ToString() + ": ");
                sb.Append(Environment.NewLine);

                foreach (var scriber in _subscribers[sub])
                {
                    sb.Append("\t" + scriber.GetType().ToString());
                    sb.Append(Environment.NewLine);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Sends the message to subscribers and cleans out null subscribers
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private void SendMessage(MessageBusMessageType message, MessageBusEventArgs args)
        {
            // check for no subscribers
            if (!_subscribers.ContainsKey(message)) return;

            // transmit
            foreach(var sub in _subscribers[message])
            {
                // check for destroyed sub
                if (sub == null) continue;

                sub.HandleMessage(message, args);
            }

            // clear out null subs
            _subscribers[message] = _subscribers[message].Where(o => o != null).ToList();
        }
    }
}
