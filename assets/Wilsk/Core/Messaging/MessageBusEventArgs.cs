﻿namespace Wilsk.Core.Messaging
{
    using System;
    using UnityEngine;

    /// <summary>
    /// A message transmitted by the message bus
    /// </summary>
    public class MessageBusEventArgs : EventArgs
    {
        #region Constructors
        public MessageBusEventArgs(MessageBusMessageType message, object objSending, object value) : base()
        {
            Message = message;
            Sender = objSending;
            Value = value;
        }
        #endregion

        /// <summary>
        /// Gets the message type being sent
        /// </summary>
        public MessageBusMessageType Message { get; private set; }

        /// <summary>
        /// Gets the GameObject that sent the message
        /// </summary>
        public object Sender { get; private set; }

        /// <summary>
        /// The value being transmitted in the event args
        /// </summary>
        public object Value { get; private set; }
        
        public T Get<T>()
        {
            return (T)Value;
        }
    }
}
