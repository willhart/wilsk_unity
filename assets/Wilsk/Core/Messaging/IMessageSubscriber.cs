﻿namespace Wilsk.Core.Messaging
{
    /// <summary>
    /// Provides a simple subscriber interface so objects can register with the 
    /// message bus and 
    /// </summary>
    public interface IMessageBusSubscriber
    {
        void Subscribe();
        void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args);
    }
}
