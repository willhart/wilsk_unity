﻿namespace Wilsk.Core.Messaging
{
    using UnityEngine;
    using Screens;
    using Zenject;

    /// <summary>
    /// A simple container class for a MessageBus so that messages can be easily hooked
    /// up from UI elements or in game
    /// </summary>
    public class MessageBusContainer : MonoBehaviour
    {
        /// <summary>
        /// The message bus for decoupled communication between components
        /// </summary>
        [Inject]
        private MessageBus _bus;

        /// <summary>
        /// Request a scene change (i.e. pause or main menu or new level etc)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="screenName"></param>
        public void TriggerSceneChange(int screenId)
        {
            var screenName = (ScreenType)screenId;
            _bus.SendMessage(null, MessageBusMessageType.ScreenChangeRequested, (int)screenName);
        }
    }
}
