﻿namespace Wilsk.Core.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// Handles local messaging within a component
    /// </summary>
    public class ComponentMessageBus : MessageBus
    {
    }
}
