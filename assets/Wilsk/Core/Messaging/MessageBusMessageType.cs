﻿namespace Wilsk.Core.Messaging
{
    /// <summary>
    /// Holds message types that can be sent by the message bus
    /// </summary>
    public enum MessageBusMessageType
    {
        ComponentDamageReceived,
        ComponentHealthUpdated,
        ComponentHealthDepleted,
        ComponentOnLeftClick,
        ComponentOnRightClick,
        ComponentPositionUpdated,
        ComponentRegisterForVisibility,
        ComponentSetSelected,
        ComponentSetInitialPosition,
        ComponentSetNewDestination,
        ComponentSetNewWaypoint,
        ComponentWaypointsUpdated,
        
        DestroyGameObject,

        PathRequested,
        PathAvailable,

        RendererRequested,

        ScreenChangeRequested,
        ScreenUnloading
    }
}
