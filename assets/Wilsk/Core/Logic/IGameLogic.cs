﻿namespace Wilsk.Core.Logic
{
    public interface IGameLogic
    {
        void Update(float deltaTime);
    }
}