﻿namespace Wilsk.Core.Logic
{
    using System;
    using Messaging;

    /// <summary>
    /// A base class for handling game logic
    /// </summary>
    public abstract class AbstractGameLogic : IGameLogic, IMessageBusSubscriber
    {
        public abstract void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args);
        public abstract void Subscribe();

        public abstract void Update(float deltaTime);
    }
}
