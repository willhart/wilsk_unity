﻿namespace Wilsk.Core.Logic
{
    using System;
    using Zenject;

    using Messaging;
    
    public class EmptyGameLogic : AbstractGameLogic
    {
        [PostInject]
        private void Init()
        {
            Subscribe();
        }

        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            return;
        }

        public override void Subscribe()
        {
            return;
        }

        public override void Update(float deltaTime)
        {
            return;
        }
    }
}
