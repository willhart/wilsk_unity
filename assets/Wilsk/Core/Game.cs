﻿namespace Wilsk.Core
{
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    using Entities;
    using Input;
    using Messaging;
    using Screens;

    /// <summary>
    /// A core game class which holds all game state and information 
    /// </summary>
    public class Game : IMessageBusSubscriber
    {

#pragma warning disable 0649
        /// <summary>
        /// Handles input for the game
        /// </summary>
        [Inject]
        private IInputProxy _input;

        /// <summary>
        /// The message bus for decoupled communication between components
        /// </summary>
        [Inject]
        private MessageBus _bus;

        /// <summary>
        /// The main menu to show first
        /// </summary>
        [Inject]
        private IMainMenu _mainMenu;

        /// <summary>
        /// A scene factory for generating scenes
        /// </summary>
        [Inject]
        private ScreenFactory _factory;
#pragma warning restore 0649

        /// <summary>
        /// Holds a stack of screens that are being displayed (using LinkedList as stack doesn't allow pop 
        /// at an arbitrary index which may be required if a scene dequeues out of the sequence in which they
        /// were pushed
        /// </summary>
        private readonly LinkedList<IScreen> _screenStack = new LinkedList<IScreen>();
        
        /// <summary>
        /// Sets up the game, injecting all required dependencies
        /// </summary>
        [PostInject]
        private void Init()
        {
            // set up the message bus
            Subscribe();
        }

        /// <summary>
        /// Loads a screen asynchronously
        /// </summary>
        /// <param name="screenType"></param>
        /// <returns></returns>
        internal IScreen GetScreen(ScreenType screenType)
        {
            return _factory.Create(screenType);
        }

        /// <summary>
        /// Starts the game running by showing the main menu and/or splash screen
        /// </summary>
        internal void BeginGame()
        { 
            // start up the main menu
            var mainMenuLevel = _mainMenu.GetScreen();
            mainMenuLevel.Configure(new LevelConfiguration());

            _bus.SendMessage(this, MessageBusMessageType.ScreenChangeRequested, (int)mainMenuLevel.ScreenType);
        }

        /// <summary>
        /// Update the game state
        /// </summary>
        internal void Update()
        {
            _input.Update();

            if (_screenStack.Count > 0)
            {
                _screenStack.First.Value.Update();
            }
        }
        
        /// <summary>
        /// Subscribes to message bus channels
        /// </summary>
        public void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ScreenChangeRequested);
            _bus.Subscribe(this, MessageBusMessageType.ScreenUnloading);
        }

        /// <summary>
        /// Handles messages that the game is subscribed to
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            IScreen screen;

            switch(type)
            {
                case MessageBusMessageType.ScreenChangeRequested:
                    // keep track of displayed screens
                    screen = GetScreen((ScreenType)args.Get<int>());

                    if (!screen.LoadCumulatively)
                    {
                        var removalList = new List<IScreen>(_screenStack); // defensive copy
                        foreach (var s in removalList)
                        {
                            s.Unload();
                        }
                    }

                    Debug.Log("Pushing screen " + screen.ScreenType.ToString() + " on to stack");
                    _screenStack.AddFirst(screen);

                    break;

                case MessageBusMessageType.ScreenUnloading:
                    screen = GetScreen((ScreenType)args.Get<int>());
                    _screenStack.Remove(screen);
                    Debug.Log("Unloaded screen " + screen.ScreenType.ToString());

                    break;

                default:
                    Debug.Log("Received unexpected message type " + type.ToString());
                    break;
            }
        }
    }
}
