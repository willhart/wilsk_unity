﻿namespace Wilsk.Core
{
    using UnityEngine;

    /// <summary>
    /// A simple class to persist an object on scene changes
    /// </summary>
    public class PersistOnSceneChange : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}