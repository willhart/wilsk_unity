﻿namespace Wilsk.Core
{
    using UnityEngine;

    /// <summary>
    /// A class for managing time which allows scaling time and pausing the game
    /// </summary>
    public class TimeKeeper
    {
        /// <summary>
        /// Initialise the time keeper class
        /// </summary>
        static TimeKeeper()
        {
            Paused = false;
            TimeScale = 1f;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the game is currently paused
        /// </summary>
        public static bool Paused { get; set; }

        /// <summary>
        /// Gets or sets the current time scale to apply
        /// </summary>
        public static float TimeScale { get; set; }

        /// <summary>
        /// Gets the time elapsed since the last updated, adjusted for the TimeScale and 
        /// set to 0 if the game is currently paused
        /// </summary>
        public static float DeltaTime
        {
            get
            {
                if (Paused)
                {
                    return 0;
                }

                return Time.deltaTime * TimeScale;
            }
        }

        /// <summary>
        /// Gets the elapsed game time, not taking pauses or time scaling into account
        /// </summary>
        public static float GameTime
        {
            get
            {
                return Time.time;
            }
        }
    }
}
