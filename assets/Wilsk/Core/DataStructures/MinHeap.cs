﻿namespace Wilsk.Core.DataStructures
{
    using System;

    /// <summary>
    /// Implemented from https://www.youtube.com/watch?v=3Dw5d7PlcTM&index=4&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW
    /// </summary>
    public class MinHeap<T> where T : IHeapItem<T>
    {
        #region Private Members
        /// <summary>
        /// The array of stored items in the heap
        /// </summary>
        private T[] _items;
        #endregion

        #region Constructors
        public MinHeap(int maxCount)
        {
            _items = new T[maxCount];
            Count = 0;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a new item to the Heap. The item class must implement the IHeapItem interface
        /// </summary>
        /// <exception cref="ArgumentException">If the heap is full</exception>
        /// <param name="item"></param>
        public void Add(T item)
        {
            if (Count == _items.Length)
            {
                throw new ArgumentException("Unable to add item to heap as the heap is full");
            }

            item.HeapIndex = Count;
            _items[Count] = item;
            SortUp(item);

            ++Count;
        }

        /// <summary>
        /// Pop the lowest priority item off the heap.
        /// </summary>
        /// <exception cref="InvalidOperationException">If the heap is empty</exception>
        /// <returns></returns>
        public T Pop()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Attempted to pop items from an empty heap");
            }

            // get the item to return
            var firstItem = _items[0];

            // reorder the heap, as long as we haven't popped the last item
            Count--;
            if (Count > 0)
            {
                _items[0] = _items[Count];
                _items[Count] = default(T); // clear previous
                _items[0].HeapIndex = 0;
                SortDown(_items[0]);
            }
            else
            {
                // clean up the items array
                _items[0] = default(T);
            }

            return firstItem;
        }

        /// <summary>
        /// Returns true if the passed item is contained within the heap
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            try
            {
                return Equals(item, _items[item.HeapIndex]);
            } 
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }

        /// <summary>
        /// Reprioritises an item in the heap
        /// </summary>
        /// <param name="item"></param>
        public void ReprioritiseItem(T item)
        {
            SortUp(item);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Recursively orts the heap by bubbling the selected item up 
        /// </summary>
        /// <param name="item"></param>
        private void SortUp(T item)
        {
            var parentIdx = (item.HeapIndex - 1) / 2;
            var parent = _items[parentIdx];

            if (item.CompareTo(parent) > 0)
            {
                Swap(item, parent);
                SortUp(item);
            }
        }

        /// <summary>
        /// Recursively sorts the heap by bubbling the selected item down
        /// </summary>
        /// <param name="item"></param>
        private void SortDown(T item)
        {
            var leftChild = item.HeapIndex * 2 + 1;
            var rightChild = leftChild + 1;
            int swapIdx = 0;

            if (leftChild < Count)
            {
                swapIdx = leftChild;
                
                if (rightChild < Count && _items[leftChild].CompareTo(_items[rightChild]) < 0)
                {
                    swapIdx = rightChild;
                }

                if (item.CompareTo(_items[swapIdx]) < 0)
                {
                    Swap(item, _items[swapIdx]);
                    SortDown(item);
                }
            }
        }

        /// <summary>
        /// Swap two given items in the heap
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        private void Swap(T a, T b)
        {
            _items[a.HeapIndex] = b;
            _items[b.HeapIndex] = a;

            var tmpIdx = a.HeapIndex;
            a.HeapIndex = b.HeapIndex;
            b.HeapIndex = tmpIdx;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the number of items currently in the heap
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Gets the storage size of the min heap
        /// </summary>
        public int Size {
            get
            {
                return _items.Length;
            }
        }
        #endregion
    }
}