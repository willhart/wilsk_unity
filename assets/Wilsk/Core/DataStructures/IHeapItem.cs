﻿namespace Wilsk.Core.DataStructures
{
    using System;

    public interface IHeapItem<T> : IComparable<T>
    {
        int HeapIndex { get; set; }
    }
}
