﻿namespace Wilsk.Core
{
    using System.Collections;
    using UnityEngine;
    using Zenject;

    using Async;
    using Messaging;
    using System;
    using Screens;

    /// <summary>
    /// A MonoBehaviour class for adding a "game" to a scene
    /// </summary>
    public class GameContainer : MonoBehaviour, IMessageBusSubscriber
    {

#pragma warning disable 0649
        /// <summary>
        /// The message bus for decoupled communication between components
        /// </summary>
        [Inject]
        private MessageBus _bus;
#pragma warning restore 0649
        /// <summary>
        /// The game that is being run
        /// </summary>
        private Game _game;

        /// <summary>
        /// Set to true when a gameObject with a GameContainer component has awoken.
        /// All other gamecontainer gameObjects will be destroyed
        /// </summary>
        private bool isSet = false;

        /// <summary>
        /// Set to true when the game has been initialised. This prevents the [PostInject]
        /// call from initialising the game state every time a new scene is injected.
        /// </summary>
        private bool isInit = false;

        /// <summary>
        /// Sets up the game container and starts the game after injection
        /// </summary>
        /// <param name="game"></param>
        [PostInject]
        private void Init(Game game)
        {
            // setup the game only the first time injection occurs
            if (!isInit)
            {
                isInit = true;
                _game = game;

                Subscribe();
                _game.BeginGame();
            }
        }

        /// <summary>
        /// Called when the game object is first created
        /// </summary>
        void Awake()
        {
            // there can only be one GameContainer 
            if (isSet)
            {
                Destroy(gameObject);
                return;
            }

            // persist the game container object on scene load - try parent object first
            // then don't destroy this object if the parent is null
            DontDestroyOnLoad(gameObject);
            isSet = true;
        }
        
        /// <summary>
        /// Called every frame to update game state
        /// </summary>
        void Update()
        {
            if (isInit)
            {
                _game.Update();
            }
        }

        /// <summary>
        /// Subscribes to MessageBus channels
        /// </summary>
        public void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ScreenChangeRequested);
        }

        /// <summary>
        /// Handles MessageBus messages
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ScreenChangeRequested)
            {
                LoadScreen((ScreenType)args.Get<int>());
            }
        }

        /// <summary>
        /// Loads a screen
        /// </summary>
        /// <param name="screenType"></param>
        private void LoadScreen(ScreenType screenType)
        {
            var screen = _game.GetScreen(screenType);
            Debug.Log("Loading " + screenType + (screen.LoadCumulatively ? ", cumulative" : ""));
            StartCoroutine(screen.Load());
        }
    }
}
