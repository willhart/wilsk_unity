﻿namespace Wilsk.Core
{
    using System;
    
    /// <summary>
    /// A custom event args class which stores level name
    /// </summary>
    public class ScreenEventArgs : EventArgs
    {
        public string LevelName;

        public ScreenEventArgs(string levelName) : base()
        {
            LevelName = levelName;
        }
    }
}
