﻿namespace Wilsk.Core.Async
{
    using UnityEngine;
    using System;
    using System.Collections;

    /// <summary>
    /// A Coroutine class which gives the ability to return values or to
    /// capture exceptions
    /// 
    /// Source: http://twistedoakstudios.com/blog/Post83_coroutines-more-than-you-want-to-know
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypedCoroutine<T>
    {
        /// <summary>
        /// The value to be returned
        /// </summary>
        private T _returnVal;

        /// <summary>
        /// Any exception that was raised during the coroutine
        /// </summary>
        private Exception _raisedException;

        /// <summary>
        /// Default constructor
        /// </summary>
        public TypedCoroutine()
        {
            IsDone = false;
        }

        /// <summary>
        /// Manage the internal coroutine
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        public IEnumerator InternalRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext())
                    {
                        IsDone = true;
                        yield break;
                    }
                }
                catch (Exception e)
                {
                    _raisedException = e;
                    IsDone = true;
                    yield break;
                }

                object yielded = coroutine.Current;
                if (yielded != null && yielded.GetType() == typeof(T))
                {
                    _returnVal = (T)yielded;
                    IsDone = true;
                    yield break;
                }
                else
                {
                    yield return coroutine.Current;
                }
            }
        }

        /// <summary>
        /// Gets the value returned by the coroutine, or throws an exception
        /// if one was raised in the coroutine
        /// </summary>
        public T Value
        {
            get
            {
                if (_raisedException != null)
                {
                    throw _raisedException;
                }
                return _returnVal;
            }
        }

        /// <summary>
        /// Gets the internal routine being run by the TypedCoroutine
        /// </summary>
        public Coroutine Routine { get; set; }

        /// <summary>
        /// Gets a value which is true if the coroutine has finished processing
        /// </summary>
        public bool IsDone { get; private set; }
    }
}
