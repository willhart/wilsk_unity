﻿namespace Wilsk.Core.Async
{
    using System;
    using System.Collections;
    using System.Threading;

    /// <summary>
    /// An asynchronous threaded job with monitoring available via coroutine
    /// Based on http://answers.unity3d.com/answers/357040/view.html
    /// </summary>
    public abstract class AbstractThreadedJob
    {
        #region Events
        /// <summary>
        /// An event triggered when the job has finished processing
        /// </summary>
        public event EventHandler ProcessingDone;
        #endregion

        #region Private Members
        /// <summary>
        /// A flag set to true when the thread has finished processing
        /// </summary>
        private bool _isDone = false;
        
        /// <summary>
        /// A lock object to allow safe access from Unity threads
        /// </summary>
        protected object _lockObject = new object();

        /// <summary>
        /// The thread the job is run on
        /// </summary>
        private Thread _thread;
        #endregion
        
        #region Public Methods
        /// <summary>
        /// Starts the thread running
        /// </summary>
        public virtual void Start()
        {
            _thread = new Thread(Run);
            _thread.Start();
        }

        /// <summary>
        /// Cancel running the thread
        /// </summary>
        public virtual void Abort()
        {
            _thread.Abort();
        }

        /// <summary>
        /// A coroutine that waits until the thread has completed
        /// </summary>
        /// <returns></returns>
        public IEnumerator WaitFor()
        {
            Start();

            while (!Update())
            {
                yield return null;
            }
        }
        #endregion

        #region Abstract Members
        /// <summary>
        /// The action the thread runs
        /// </summary>
        protected abstract void ThreadFunction();

        /// <summary>
        /// Called when the thread has finished processing
        /// </summary>
        protected abstract void OnFinished();
        #endregion

        #region Private Members
        /// <summary>
        /// Runs the thread operation
        /// </summary>
        private void Run()
        {
            ThreadFunction();
            IsDone = true;
        }

        /// <summary>
        /// Periodically check if the thread has finished processing. If it has
        /// call the completion callback
        /// </summary>
        /// <returns></returns>
        private bool Update()
        {
            if (IsDone)
            {
                OnFinished();

                if (ProcessingDone != null)
                {
                    ProcessingDone(this, new EventArgs());
                }

                return true;
            }
            return false;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a flag indicating if the thread work is done. Threadsafe.
        /// </summary>
        public bool IsDone
        {
            get
            {
                lock (_lockObject)
                {
                    return _isDone == true;
                }
            }
            set
            {
                lock (_lockObject)
                {
                    _isDone = value;
                }
            }
        }
        #endregion
    }
}