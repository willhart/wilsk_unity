﻿namespace Wilsk.Core.Async
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides a queue of threaded jobs and limits the number 
    /// of simultaneous jobs run
    /// </summary>
    public class AsynchronousJobQueue : MonoBehaviour
    {
        #region Private Members
        /// <summary>
        /// The queue of jobs to be processed
        /// </summary>
        private readonly Queue<AbstractThreadedJob> _jobs = new Queue<AbstractThreadedJob>();

        /// <summary>
        /// The number of jobs currently being processed
        /// </summary>
        private int _currentJobCount;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="maxThreads"></param>
        public AsynchronousJobQueue()
        {
            MaxThreads = 5;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a job to the processing queue
        /// </summary>
        /// <param name="job"></param>
        public void AddJob(AbstractThreadedJob job)
        {
            _jobs.Enqueue(job);
            job.ProcessingDone += HandleProcessingComplete;

            RunNewJobs();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// An event handler for when a job has finished processing. Potentially 
        /// triggers new jobs to be run
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleProcessingComplete(object sender, System.EventArgs e)
        {
            var job = sender as AbstractThreadedJob;
            job.ProcessingDone -= HandleProcessingComplete;

            --_currentJobCount;

            RunNewJobs();
        }

        /// <summary>
        /// Runs new jobs if any are available to run
        /// </summary>
        private void RunNewJobs()
        {
            while (_currentJobCount < MaxThreads && _jobs.Count > 0)
            {
                var job = _jobs.Dequeue();
                ++_currentJobCount;

                StartCoroutine(job.WaitFor());
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The maximum number of threads to operate
        /// </summary>
        public int MaxThreads { get; set; }
        #endregion
    }
}
