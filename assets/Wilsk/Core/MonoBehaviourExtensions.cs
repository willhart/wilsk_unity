﻿namespace Wilsk.Core
{
    using System.Collections;
    using UnityEngine;

    using Async;

    /// <summary>
    /// Give MonoBehaviours the ability to launch TypedCoroutines
    /// </summary>
    public static class MonoBehaviourExtensions
    {   
        /// <summary>
        /// Start and return a TypedCoroutine
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        public static TypedCoroutine<T> StartTypedCoroutine<T>(this MonoBehaviour obj, IEnumerator coroutine)
        {
            var coroutineObject = new TypedCoroutine<T>();
            coroutineObject.Routine = obj.StartCoroutine(coroutineObject.InternalRoutine(coroutine));
            return coroutineObject;
        }
    }
}
