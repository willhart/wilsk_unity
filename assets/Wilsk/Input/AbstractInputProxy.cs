﻿namespace Wilsk.Input
{
    using UnityEngine;

    public abstract class AbstractInputProxy : IInputProxy
    {
        public Vector3 PointerScreenLocation { get; protected set; }

        public abstract void Update();

        public abstract float GetAnalog(InputControlTypes input);

        public abstract bool GetDigital(InputControlTypes input);
    }
}
