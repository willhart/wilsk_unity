﻿namespace Wilsk.Input
{
    using UnityEngine;

    public interface IInputProxy
    {
        Vector3 PointerScreenLocation { get; }

        float GetAnalog(InputControlTypes input);
        bool GetDigital(InputControlTypes input);

        void Update();
    }
}
