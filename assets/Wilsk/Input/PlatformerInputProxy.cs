﻿namespace Wilsk.Input
{
    using UnityEngine;

    /// <summary>
    /// An input proxy for a platformer game
    /// </summary>
    public class PlatformerInputProxy : AbstractInputProxy
    {
        /// <summary>
        /// Has the move button been pressed?
        /// </summary>
        private float _horizontal = 0;

        /// <summary>
        /// Has the jump button been pressed?
        /// </summary>
        private bool _jump = false;

        /// <summary>
        /// Has the fire button been pressed?
        /// </summary>
        private bool _fire = false;

        /// <summary>
        /// Called by owning class every frame before input is accessed
        /// </summary>
        public override void Update()
        {
            _horizontal = Input.GetAxis("Horizontal");
            _jump = Input.GetButtonDown("Jump");
            _fire = Input.GetButtonDown("Fire");
        }

        /// <summary>
        /// Returns analog input - there isn't any as its all mouse controlled
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override float GetAnalog(InputControlTypes input)
        {
            switch(input)
            {
                case InputControlTypes.Horizontal:
                    return _horizontal;
                case InputControlTypes.Left:
                    return _horizontal < 0 ? _horizontal : 0;
                case InputControlTypes.Right:
                    return _horizontal > 0 ? _horizontal : 0;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Gets digital input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override bool GetDigital(InputControlTypes input)
        {
            switch (input)
            {
                case InputControlTypes.Jump:
                    return _jump;
                case InputControlTypes.Fire:
                    return _fire;
                default:
                    return false;
            }
        }
    }
}
