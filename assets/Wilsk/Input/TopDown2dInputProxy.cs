﻿namespace Wilsk.Input
{
    using UnityEngine;

    public class TopDown2dInputProxy : AbstractInputProxy
    {
        /// <summary>
        /// Has the left mouse button been clicked?
        /// </summary>
        private bool _leftClicked = false;

        /// <summary>
        /// Has the right mouse button been clicked?
        /// </summary>
        private bool _rightClicked = false;

        /// <summary>
        /// Is left shift held down?
        /// </summary>
        private bool _leftShift = false;

        /// <summary>
        /// Is right shift held down?
        /// </summary>
        private bool _rightShift = false;

        /// <summary>
        /// Called by owning class every frame before input is accessed
        /// </summary>
        public override void Update()
        {
            _leftClicked = Input.GetButtonDown("LeftClick");
            _rightClicked = Input.GetButtonDown("RightClick");

            _leftShift = Input.GetButton("LeftShift");
            _rightShift = Input.GetButton("RightShift");

            PointerScreenLocation = Input.mousePosition;
        }

        /// <summary>
        /// Returns analog input - there isn't any as its all mouse controlled
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override float GetAnalog(InputControlTypes input)
        {
            return 0; 
        }

        /// <summary>
        /// Gets digital input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override bool GetDigital(InputControlTypes input)
        {
            switch(input)
            {
                case InputControlTypes.LeftClick:
                    return _leftClicked;
                case InputControlTypes.RightClick:
                    return _rightClicked;
                case InputControlTypes.LeftShift:
                    return _leftShift;
                case InputControlTypes.RightShift:
                    return _rightShift;
                default:
                    return false;
            }
        }
    }
}
