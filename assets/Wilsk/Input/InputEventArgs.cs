﻿namespace Wilsk.Input
{
    using System;
    using UnityEngine;

    public class InputEventArgs : EventArgs
    {
        public Vector3 Location;
        public bool UseModifier;
    }
}
