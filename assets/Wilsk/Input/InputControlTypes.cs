﻿namespace Wilsk.Input
{
    public enum InputControlTypes
    {
        Left,
        Up,
        Right,
        Down,
        Fire,
        Move,
        Jump,
        LeftClick,
        RightClick,
        LeftShift,
        RightShift,
        Horizontal,
        Vertical
    }
}
