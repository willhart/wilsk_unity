﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;

    using Core.Async;
    using Core.Messaging;

    /// <summary>
    /// Generates a path asynchronously on a separate thread, returning the result when finished
    /// </summary>
    public class PathfindingThreadedJob : AbstractThreadedJob
    {
        #region Private Members
        /// <summary>
        /// The desired start position
        /// </summary>
        private Vector3 _start;

        /// <summary>
        /// The desired target position
        /// </summary>
        private Vector3 _target;

        /// <summary>
        /// The graph to use for path finding
        /// </summary>
        private Graph _graph;

        /// <summary>
        /// The path found
        /// </summary>
        private List<Vector3> _path;

        /// <summary>
        /// The Wilsk navigation agent a path is being defined for
        /// </summary>
        private INavigationAgent _agent;

        /// <summary>
        /// The message bus subscriber that requested the path
        /// </summary>
        private MessageBus _requestingBus;
        #endregion

        #region Constructor
        public PathfindingThreadedJob(Graph graph, Vector3 startPos, Vector3 targetPos, INavigationAgent agent) : base()
        {
            _graph = graph;
            _start = startPos;
            _target = targetPos;
            _agent = agent;
        }

        public PathfindingThreadedJob(PathfindingRequestArgs request) : base()
        {
            _graph = request.Graph;
            _start = request.Origin;
            _target = request.Destination;
            _requestingBus = request.Requester;
        }
        #endregion

        #region Private Abstract Implementation
        /// <summary>
        /// Sets the navigation agent to use the given path
        /// </summary>
        protected override void OnFinished()
        {
            if (_agent == null)
            {
                _requestingBus.SendMessage(this, MessageBusMessageType.PathAvailable, _path);
            }
            else
            {
                _agent.SetWaypoints(_path);
            }
        }

        /// <summary>
        /// Finds a path on a separate thread
        /// </summary>
        protected override void ThreadFunction()
        {
            _path = _graph.GetPathSynchronous(_start, _target);
        }
        #endregion
    }
}
