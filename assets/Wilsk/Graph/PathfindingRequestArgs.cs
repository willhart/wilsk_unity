﻿namespace Wilsk.Graph
{
    using UnityEngine;
    using Core.Messaging;

    /// <summary>
    /// Contains information about a pathfinding request
    /// </summary>
    public struct PathfindingRequestArgs
    {
        public MessageBus Requester;
        public Graph Graph;
        public Vector3 Origin;
        public Vector3 Destination;
    }
}
