﻿namespace Wilsk.Graph
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// Builds a graph from a string representation
    /// </summary>
    internal class GraphParser
    {
        /// <summary>
        /// The data that contains the graph information
        /// </summary>
        private IEnumerable<string> _lines;

        /// <summary>
        /// The point that splits between the movement penalties and the heighmap
        /// </summary>
        private int _midIndex = -1;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="data"></param>
        public GraphParser(string data)
        {
            // split out the lines
            _lines = data.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            // get the size from the first line
            var sizeLine = _lines.ElementAt(0);
            SetSize(sizeLine);

            // ignore the first line
            _lines = _lines.Skip(1);

            // find the split point
            var i = 0;
            foreach (var line in _lines)
            {
                if (line == string.Empty)
                {
                    _midIndex = i;
                    break;
                }
                ++i;
            }

            if (_midIndex == -1)
            {
                throw new ArgumentException("Improperly formatted map string, unable to process");
            }
        }

        /// <summary>
        /// Returns an empty grid
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <returns></returns>
        public Node[,] BuildGraph(float nodeRadius, bool is2dMode, Func<int, int, Vector3> getWorldPosition)
        {
            var graphLines = _lines.Take(_midIndex).ToArray();
            var lineCount = graphLines.Length;

            if (lineCount != GridHeight)
            {
                throw new InvalidOperationException("The number of lines in the graph doesn't match the y-size of the grid. Unable to proceed");
            }

            var grid = new Node[GridWidth, GridHeight];

            for (var y = 0; y < lineCount; ++y)
            {
                var items = graphLines[y].Split(',');
                var itemCount = items.Length;

                if (itemCount != GridWidth)
                {
                    throw new InvalidOperationException(string.Format(
                        "Line {0} in graph definition doesn't have the required number of elements. Expected {1}, found {2}",
                        y + 1, GridWidth, itemCount));
                }

                for (var x = 0; x < itemCount; ++x)
                {
                    int node;

                    if (!int.TryParse(items[x], out node))
                    {
                        node = -1;
                        Debug.LogWarning(string.Format("Unable to parse integer at {0},{1}, (data was {2}) using -1, unwalkable", x, y, items[x]));
                    }

                    var pos = getWorldPosition(x, y);
                    var n = new Node(x, y, node != -1, pos);
                    grid[x, y] = n;
                }
            }

            return grid;
        }

        /// <summary>
        /// Builds an empty height map with everything at height 0
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <returns></returns>
        public byte[,] BuildHeightMap()
        {
            var hm = new byte[GridWidth, GridHeight];
            var graphLines = _lines.Skip(_midIndex + 1).ToArray();
            var lineCount = graphLines.Length;

            if (lineCount != GridHeight)
            {
                throw new InvalidOperationException("The number of lines in the height map doesn't match the y-size of the grid. Unable to proceed");
            }

            for (var y = 0; y < lineCount; ++y)
            {
                var items = graphLines[y].Split(',');
                var itemCount = items.Length;

                if (itemCount != GridWidth)
                {
                    throw new InvalidOperationException(string.Format(
                        "Line {0} in height map definition doesn't have the required number of elements. Expected {1}, found {2}",
                        y + 1, GridWidth, itemCount));
                }

                for (var x = 0; x < itemCount; ++x)
                {
                    byte node;
                    if (!byte.TryParse(items[x], out node))
                    {
                        node = 0; // not really required but hey
                        Debug.LogWarning(string.Format("Unable to parse byte at {0},{1}, (data was {2}) using 0", x, y, items[x]));
                    }

                    hm[x, y] = node;
                }
            }

            return hm;
        }

        /// <summary>
        /// Parses the first line of a map to retrieve the x-y size. Sizes are in the format "[X size]x[Y Size]\n"
        /// </summary>
        /// <param name="sizeLine"></param>
        private void SetSize(string sizeLine)
        {
            if (!sizeLine.Contains("x"))
            {
                throw new ArgumentException("Provided map doesn't have the size in the first line. Unable to parse");
            }

            var lineParts = sizeLine.Split('x');

            int result;
            if (int.TryParse(lineParts[0], out result))
            {
                GridWidth = result;
            }
            else
            {
                throw new ArgumentException(string.Format("Unable to parse the width of the size line - {0} doesn't convert to a number", lineParts[0]));
            }

            if (int.TryParse(lineParts[1], out result))
            {
                GridHeight = result;
            }
            else
            {
                throw new ArgumentException(string.Format("Unable to parse the height of the size line - {0} doesn't convert to a number", lineParts[1]));
            }
        }

        /// <summary>
        /// Gets the number of tiles in the x-direction of the grid
        /// </summary>
        public int GridWidth { get; private set; }

        /// <summary>
        /// Gets the number of tiles in the y-direction of the grid
        /// </summary>
        public int GridHeight { get; private set; }
    }
}
