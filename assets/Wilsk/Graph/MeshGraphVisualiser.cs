﻿namespace Wilsk.Graph
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class MeshGraphVisualiser
    {
        /// <summary>
        /// The maximum height to draw a point from the heightmap (i.e. the height a value of 255 in 
        /// the heightmap is rendered at)
        /// </summary>
        public static readonly float MaxMeshHeight = 20f;

        /// <summary>
        /// Build a game object with the generated mesh for the supplied height map added
        /// </summary>
        /// <param name="heightMap"></param>
        /// <param name="xDim"></param>
        /// <param name="yDim"></param>
        /// <param name="applyGradient"></param>
        /// <returns></returns>
        internal GameObject GenerateMesh(byte[,] heightMap, float xDim, float yDim,bool applyGradient = true)
        {
            // create the container game object for the mesh
            var go = new GameObject("terrain_mesh", new Type[]
            {
                typeof(MeshFilter),
                typeof(MeshRenderer)
            });

            // generate the mesh
            var mesh = BuildMesh(heightMap, xDim, yDim, applyGradient);
            mesh.RecalculateNormals();
            go.GetComponent<MeshFilter>().mesh = mesh;

            // build and assign a material
            var mr = go.GetComponent<MeshRenderer>();
            mr.material = new Material(Shader.Find("Standard"));
            mr.material.SetTexture("_MainTex", CreateTexture(heightMap));

            // position and rotate the mesh for 2d camera
            go.transform.Rotate(new Vector3(-90, 0, 0));

            return go;
        }

        /// <summary>
        /// Generates a mesh from a graph
        /// <param name="heightMap">The height map 0-255 of the grid</param>
        /// <param name="xDim">The x-dimension of mesh tiles</param>
        /// <param name="yDim">The y-dimension of mesh tiles</param>
        /// <param name="applyGradient">Should a smoothing gradient be applied to changes in height in the mesh?</param>
        /// <returns>A populated mesh object</returns>
        /// </summary>
        private Mesh BuildMesh(byte[,] heightMap, float xDim, float yDim, bool applyGradient)
        {
            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var uvs = new List<Vector2>();

            var xLen = heightMap.GetLength(0);
            var yLen = heightMap.GetLength(1);

            // create each block
            for (var x = 0; x < xLen; ++x)
            {
                for (var y = 0; y < yLen; ++y)
                {
                    CreateTopFace(x, y, xDim, yDim, xLen, yLen, heightMap[x, y], vertices, triangles, uvs);
                    CreateSideFaces(heightMap, x, y, xDim, yDim, xLen, yLen, vertices, triangles, uvs);
                }
            }

            var mesh = new Mesh();
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.uv = uvs.ToArray();
            
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.Optimize();

            return mesh;
        }

        /// <summary>
        /// Creates the top face of the mesh, the "terrain surface"
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="h"></param>
        /// <param name="vertices"></param>
        /// <param name="triangles"></param>
        /// <param name="uvs"></param>
        private void CreateTopFace(float x, float y, float xTileSize, float yTileSize, 
            int xGridSize, int yGridSize,
            float h, List<Vector3> vertices, List<int> triangles, List<Vector2> uvs)
        {
            var lastVec = vertices.Count;

            // scale h 0 to 10
            h = MaxMeshHeight * (h / 255f);

            vertices.Add(new Vector3(x * xTileSize        , h, y * yTileSize        ));
            vertices.Add(new Vector3(x * xTileSize        , h, y * yTileSize + yTileSize));
            vertices.Add(new Vector3(x * xTileSize + xTileSize, h, y * yTileSize + yTileSize));
            vertices.Add(new Vector3(x * xTileSize + xTileSize, h, y * yTileSize        ));

            triangles.Add(lastVec + 1);
            triangles.Add(lastVec + 2);
            triangles.Add(lastVec + 3);

            triangles.Add(lastVec);
            triangles.Add(lastVec + 1);
            triangles.Add(lastVec + 3);

            uvs.Add(new Vector2(x / xGridSize, y / yGridSize));
            uvs.Add(new Vector2(x / xGridSize, (y + 1 )/ yGridSize));
            uvs.Add(new Vector2((x + 1) / xGridSize, (y + 1) / yGridSize));
            uvs.Add(new Vector2((x + 1) / xGridSize, y / yGridSize));
        }

        /// <summary>
        /// Create a face on the side of the mesh
        /// </summary>
        /// <param name="heightMap"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="xTileSize"></param>
        /// <param name="yTileSize"></param>
        /// <param name="vertices"></param>
        /// <param name="triangles"></param>
        private void CreateSideFaces(byte[,] heightMap, float x, float y, float xTileSize, float yTileSize,
            int xGridSize, int yGridSize, List<Vector3> vertices, List<int>triangles, List<Vector2> uvs)
        {
            var xGrid = (int)x;
            var yGrid = (int)y;
            var currH = MaxMeshHeight * (heightMap[xGrid, yGrid] / 255f);// scale h 0 to 10

            var maxX = heightMap.GetLength(0);
            var maxY = heightMap.GetLength(1);

            // move around our current x,y point in the cardinal directions and draw a 
            // sideways facing quad only if the adjacent section is taller
            for (var i = -1; i <= 1; ++i)
            {
                for (var j = -1; j <= 1; ++j)
                {
                    if (i == 0  && j == 0) continue; // ignore current cell
                    if (i != 0 && j != 0) continue; // don't check diagonals
                    if (xGrid + i < 0 || xGrid + i >= maxX) continue; // check x bounds
                    if (yGrid + j < 0 || yGrid + j >= maxY) continue; // check y boubds

                    var nextH = MaxMeshHeight * (heightMap[xGrid + i, yGrid + j] / 255f); // scale h 0 to 10
                    if (nextH <= currH) continue; // only draw up to higher grids

                    // ok we need to add a quad
                    var lastVec = vertices.Count;
                    
                    if (j == 0) // we are adding in the x direction
                    {
                        var x1 = i < 0 ? x * xTileSize : (x + 1) * xTileSize;

                        vertices.Add(new Vector3(x1, currH, y * yTileSize));
                        vertices.Add(new Vector3(x1, currH, y * yTileSize + yTileSize));
                        vertices.Add(new Vector3(x1, nextH, y * yTileSize + yTileSize));
                        vertices.Add(new Vector3(x1, nextH, y * yTileSize));

                        uvs.Add(new Vector2(x1 / xGridSize, y / yGridSize));
                        uvs.Add(new Vector2(x1 / xGridSize, (y + 1) / yGridSize));
                        uvs.Add(new Vector2(x1 / xGridSize, (y + 1) / yGridSize));
                        uvs.Add(new Vector2(x1 / xGridSize, y / yGridSize));
                    }
                    else // add in the z direction
                    {
                        var y1 = j < 0 ? y * yTileSize : (y + 1) * yTileSize;

                        vertices.Add(new Vector3(x * xTileSize, currH, y1));
                        vertices.Add(new Vector3(x * xTileSize, nextH, y1));
                        vertices.Add(new Vector3(x * xTileSize + xTileSize, nextH, y1));
                        vertices.Add(new Vector3(x * xTileSize + xTileSize, currH, y1));

                        uvs.Add(new Vector2(x / xGridSize, y1 / yGridSize));
                        uvs.Add(new Vector2(x / xGridSize, y1 / yGridSize));
                        uvs.Add(new Vector2((x + 1) / xGridSize, y1 / yGridSize));
                        uvs.Add(new Vector2((x + 1) / xGridSize, y1 / yGridSize));
                    }

                    if (i > 0 || j > 0)
                    {
                        triangles.Add(lastVec + 1);
                        triangles.Add(lastVec + 2);
                        triangles.Add(lastVec + 3);

                        triangles.Add(lastVec);
                        triangles.Add(lastVec + 1);
                        triangles.Add(lastVec + 3);
                    }
                    else
                    {
                        triangles.Add(lastVec + 3);
                        triangles.Add(lastVec + 2);
                        triangles.Add(lastVec + 1);

                        triangles.Add(lastVec + 3);
                        triangles.Add(lastVec + 1);
                        triangles.Add(lastVec);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a 2d texture with the colour of each node reflecting its height value in the heightmap
        /// </summary>
        /// <param name="heightMap"></param>
        /// <returns></returns>
        private Texture2D CreateTexture(byte[,] heightMap)
        {
            var xMax = heightMap.GetLength(0);
            var yMax = heightMap.GetLength(1);
            
            // go through the heightmap and set up the texture
            var tex = new Texture2D(xMax, yMax);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.anisoLevel = 0;

            for (var x = 0; x < xMax; ++x)
            {
                for (var y = 0; y < yMax; ++y)
                {
                    tex.SetPixel(x, y, GridColourMap.GetColour(heightMap[x, y] / 255f));
                }
            }

            tex.Apply();

            return tex;
        }
    }
}
