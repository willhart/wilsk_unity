﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;
    using Core.DataStructures;

    /// <summary>
    /// An implementation of A* path finding based on 
    /// https://www.youtube.com/watch?v=nhiFx28e7JY&index=2&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW
    /// </summary>
    public class AStarPathFinder : AbstractPathFinder
    {
        /// <summary>
        /// Finds a path between two points using the A* pathfinding algorithm.
        /// 
        /// Returns null if no path is found
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="startPos"></param>
        /// <param name="targetPos"></param>
        public override List<Vector3> FindPath(Graph grid, Vector3 startPos, Vector3 targetPos)
        {
            var startNode = grid.ConvertWorldCoordinatesToNode(startPos);
            var targetNode = grid.ConvertWorldCoordinatesToNode(targetPos);

            if (!startNode.Walkable || !targetNode.Walkable)
            {
                return null;
            }

            var openSet = new MinHeap<Node>((int)(grid.WorldSize.x * grid.WorldSize.y));
            var closedSet = new HashSet<Node>();

            openSet.Add(startNode);

            // find a path
            while (openSet.Count > 0)
            {
                var current = openSet.Pop();
                closedSet.Add(current);

                // check for end condition
                if (current == targetNode)
                {
                    break;
                }

                var neighbours = grid.GetNodeNeighbours(current);
                foreach (var node in neighbours)
                {
                    if (!node.Walkable || closedSet.Contains(node)) continue;
                    
                    // cost of movement includes absolute delta height
                    var newMoveCost = current.G + Node.Distance(current, node) + Mathf.Abs(grid.HeightMap[node.X, node.Y] - grid.HeightMap[current.X, current.Y]);

                    if (newMoveCost < node.G || !openSet.Contains(node))
                    {
                        node.G = newMoveCost;
                        node.H = Node.Distance(node, targetNode);
                        node.Parent = current;

                        if (!openSet.Contains(node))
                        {
                            openSet.Add(node);
                        }

                        openSet.ReprioritiseItem(node);
                    }
                }
            }

            var path = RetracePath(startNode, targetNode);
            var simplified = SimplifyPath(path);
            return simplified;
        }

        /// <summary>
        /// Returns a list of nodes giving a path between two points in the grid
        /// Checks for non-existent paths and also circular paths. 
        /// 
        /// Returns null if no valid path is found
        /// </summary>
        /// <param name="start"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private List<Node> RetracePath(Node start, Node target)
        {
            var path = new List<Node>();
            var current = target;
            var visited = new List<Node>();

            // trace the path backwards from the target to the start
            while (current != start && current != null)
            {
                if (visited.Contains(current))
                {
                    // circular loop, return
                    return null;
                }

                // build the the path by prepending the current node
                path.Insert(0, current);
                visited.Add(current);
                current = current.Parent;
            }

            path.Insert(0, start);

            // check if we could find a path, which is a result of:
            if (path.Count == 1 || // only start in the path
                path[path.Count - 1] != target) // last item is not the target
            {
                return null;
            }

            // return the path
            return path;
        }
    }
}
