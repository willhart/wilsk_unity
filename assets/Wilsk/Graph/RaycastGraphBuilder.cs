﻿namespace Wilsk.Graph
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Builds up a graph by spherecasting at points on the grid and checking for collisions with
    /// unwalkable areas
    /// </summary>
    public class RaycastGraphBuilder : IGraphBuilder
    {
        /// <summary>
        /// The layers considered unwalkable
        /// </summary>
        private LayerMask _unwalkable;
        
        /// <summary>
        /// Returns an empty grid
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <returns></returns>
        public Node[,] BuildGraph(int sizeX, int sizeY, float nodeRadius, bool is2dMode, Func<int, int, Vector3> getWorldPosition)
        {
            var grid = new Node[sizeX, sizeY];

            for (var x = 0; x < sizeX; ++x)
            {
                for (var y = 0; y < sizeY; ++y)
                {
                    var pos = getWorldPosition(x, y);
                    var walkable = !Physics.CheckSphere(pos, nodeRadius, _unwalkable);

                    grid[x, y] = new Node(x, y, walkable, pos);
                }
            }

            return grid;
        }

        /// <summary>
        /// Builds an empty height map with everything at height 0
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <returns></returns>
        public byte[,] BuildHeightMap(int sizeX, int sizeY)
        {
            var hm = new byte[sizeX, sizeY];
            for (var i = 0; i < sizeX; ++i)
            {
                for (var j = 0; j < sizeY; ++j)
                {
                    hm[i, j] = 0;
                }
            }

            return hm;
        }

        /// <summary>
        /// For the empty graph builder, this doesn't do anything - it just returns
        /// an empty grid
        /// </summary>
        /// <param name="inputData"></param>
        public void Configure(LayerMask unwalkable, string filePath)
        {
            _unwalkable = unwalkable;
        }
    }
}
