﻿namespace Wilsk.Graph
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// A simple navigation agent for a 2d space
    /// </summary>
    public class NavigationAgent2D : INavigationAgent
    {
        #region Private Members
        /// <summary>
        /// The distance considered "close" or "arrived at" a location
        /// </summary>
        private static readonly float DeltaDistance = 0.1f;

        /// <summary>
        /// The waypoints currently in the unit's path
        /// </summary>
        private Queue<Vector3> _waypoints;

        /// <summary>
        /// The current waypoint we are navigating to
        /// </summary>
        private Vector3 _currentWaypoint = Vector3.zero;
        #endregion

        #region Public Methods
        public void SetWaypoints(List<Vector3> waypoints)
        {
            _waypoints = new Queue<Vector3>(waypoints);
        }

        public Vector3 GetNextWaypoint(Vector3 currentPosition)
        {
            // check if we have no waypoints to give
            if (_currentWaypoint == Vector3.zero)
            {
                // check if a waypoint exists
                if (_waypoints == null || _waypoints.Count == 0)
                {
                    _currentWaypoint = Vector3.zero;
                }
                else
                {
                    _currentWaypoint = _waypoints.Dequeue();
                }
            }
            

            // check if we have reached our current waypoint
            // Do nothing this frame (i.e. no waypoint), but get one on next pass
            if (Vector3.Distance(currentPosition, _currentWaypoint) < DeltaDistance)
            {
                _currentWaypoint = Vector3.zero; 
            }

            return _currentWaypoint;
        }
        #endregion
    }
}
