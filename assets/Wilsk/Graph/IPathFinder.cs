﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IPathFinder
    {
        List<Vector3> FindPath(Graph grid, Vector3 startPos, Vector3 targetPos);
        List<Vector3> FindPath(PathfindingRequestArgs args);
    }
}
