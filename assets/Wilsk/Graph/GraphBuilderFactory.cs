﻿namespace Wilsk.Graph
{
    using System;

    /// <summary>
    /// A simple factory for returning new graph builder instances. No need for 
    /// dependency injection here as the classes are all standalones
    /// </summary>
    public class GraphBuilderFactory
    {
        public IGraphBuilder Get(GraphBuilderType type)
        {
            if (type == GraphBuilderType.Empty)
            {
                return new EmptyGraphBuilder();
            }

            if (type == GraphBuilderType.Raycast)
            {
                return new RaycastGraphBuilder();
            }

            if (type == GraphBuilderType.File)
            {
                return new FileGraphBuilder();
            }

            if (type == GraphBuilderType.String)
            {
                return new StringGraphBuilder();
            }

            if (type == GraphBuilderType.UnityResource)
            {
                return new UnityResourceFileGraphBuilder();
            }

            throw new ArgumentException("Unknown graph builder type, aborting. " + type.ToString());
        }
    }
}
