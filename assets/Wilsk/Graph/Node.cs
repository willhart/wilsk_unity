﻿namespace Wilsk.Graph
{
    using UnityEngine;
    using Core.DataStructures;

    /// <summary>
    /// Holds graph information for pathfinding purposes
    /// Draws inspiration from https://www.youtube.com/watch?v=nhiFx28e7JY&index=2&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW
    /// </summary>
    public class Node : IHeapItem<Node>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="walkable"></param>
        /// <param name="worldPos"></param>
        public Node(int x, int y, bool walkable, Vector3 worldPos)
        {
            X = x;
            Y = y;
            Walkable = walkable;
            WorldPosition = worldPos;
        }

        /// <summary>
        /// Gets the distance between two nodes in grid distance
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Distance(Node a, Node b)
        {
            var dstX = Mathf.Abs(a.X - b.X);
            var dstY = Mathf.Abs(a.Y - b.Y);

            if (dstX > dstY)
            {
                return 14 * dstY + 10 * (dstX - dstY);
            }

            return 14 * dstX + 10 * (dstY - dstX);
        }

        /// <summary>
        /// Compares the priority of the two nodes
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Node other)
        {
            if (F == other.F)
            {
                return -(H.CompareTo(other.H));
            }

            return -(F.CompareTo(other.F));
        }

        /// <summary>
        /// Gets the X coordinate in node coordinates for this node
        /// </summary>
        public int X { get; private set; }

        /// <summary>
        /// Gets the Y coordinate in node coordinates for this node
        /// </summary>
        public int Y { get; private set; }

        /// <summary>
        /// The 2d/3d world position of the node
        /// </summary>
        public Vector3 WorldPosition { get; set; }

        /// <summary>
        /// Is the node walkable?
        /// </summary>
        public bool Walkable { get; private set; }
        
        /// <summary>
        /// Gets or sets the G cost during AStar Path Finding
        /// </summary>
        public int G { get; set; }

        /// <summary>
        /// Gets or sets the H cost during AStar Path Finding
        /// </summary>
        public int H { get; set; }

        /// <summary>
        /// Gets the F cost during AStar Path Finding, which is G + H
        /// </summary>
        public int F
        {
            get
            {
                return G + H;
            }
        }

        /// <summary>
        /// A parent node in the given path
        /// </summary>
        public Node Parent { get; set; }

        /// <summary>
        /// Gets or sets the heap index for this node item
        /// </summary>
        public int HeapIndex { get; set; }
    }
}