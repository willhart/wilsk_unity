﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Gets a colour based on a predefined colour range
    /// </summary>
    internal static class GridColourMap
    {
        private static List<float> _dictKeys = new List<float>()
        {
            0, 0.05f, 0.15f, 0.4f, 0.8f, 0.9f, 1f
        };

        private static Dictionary<float, Color> _colours = new Dictionary<float, Color>()
        {
            { _dictKeys[0], new Color(0.294f, 0.518f, 1) }, // blue (water)
            { _dictKeys[1], new Color(1, 0.998f, 0.502f) }, // yellow (sand)
            { _dictKeys[2], new Color(0.690f, 1, 0.345f) }, // green (grass)
            { _dictKeys[3], new Color(0.824f, 1, 0.435f) }, // yellow-gree (plains)
            { _dictKeys[4], Color.gray }, // grey (rocks)
            { _dictKeys[5], new Color(0.3f, 0.3f, 0.3f) }, // dark grey (cliffs)
            { _dictKeys[6], Color.white } // white (snow)
        };

        /// <summary>
        /// Gets a colour based on the 0-1 proportion of the height range that this
        /// level represents
        /// </summary>
        /// <param name="proportion"></param>
        /// <returns></returns>
        internal static Color GetColour(float proportion)
        {
            // handle exact matches
            var idx = _dictKeys.IndexOf(proportion);
            if (idx >= 0)
            {
                return _colours[_dictKeys[idx]];
            }

            // find bracketing indices
            var prior = 0;

            for (var i = prior; i < _dictKeys.Count - 2; ++i)
            {
                if (proportion < _dictKeys[i + 1])
                {
                    break;
                }

                ++prior;
            }

            return Color.Lerp(_colours[_dictKeys[prior]], _colours[_dictKeys[prior + 1]], proportion);
        }
    }
}
