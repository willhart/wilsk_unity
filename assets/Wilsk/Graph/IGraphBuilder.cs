﻿namespace Wilsk.Graph
{
    using System;
    using UnityEngine;

    public interface IGraphBuilder
    {
        void Configure(LayerMask unwalkable, string filePath);
        Node[,] BuildGraph(int sizeX, int sizeY, float nodeRadius, bool is2dMode, Func<int, int, Vector3> getWorldPosition);
        byte[,] BuildHeightMap(int sizeX, int sizeY);
    }
}
