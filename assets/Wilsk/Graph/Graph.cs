﻿namespace Wilsk.Graph
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    using Core.Async;
    using Core.Messaging;

    /// <summary>
    /// A simple graph class used to hold a game grid and perform various operations on it. 
    /// Points in the grid are referenced from the bottom left of the nodes.
    /// 
    /// Draws inspiration from https://www.youtube.com/watch?v=nhiFx28e7JY&index=2&list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW
    /// </summary>
    public class Graph : IMessageBusSubscriber
    {
        #region Private Members
        /// <summary>
        /// Holds the nodes that make up the grid
        /// </summary>
        private Node[,] _grid;

        /// <summary>
        /// Holds the nodes that make up the grid
        /// </summary>
        private byte[,] _heightMap;

        /// <summary>
        /// The number of nodes in the x direction
        /// </summary>
        private int _nodesInX;

        /// <summary>
        /// The number of nodes in the y direction
        /// </summary>
        private int _nodesInY;

        /// <summary>
        /// An injected graph builder (setup in the Init private method)
        /// </summary>
        private GraphBuilderFactory _factory;

        /// <summary>
        /// The path finder to use for generating paths
        /// </summary>
        private IPathFinder _pathFinder;

        /// <summary>
        /// The job queue used to asynchronously process pathfinding requests
        /// </summary>
        private AsynchronousJobQueue _jobQueue;

        /// <summary>
        /// A message bus for receiving pathfinding events
        /// </summary>
        [Inject]
        private MessageBus _bus;

        /// <summary>
        /// The visualiser usd for drawing the mesh
        /// </summary>
        private MeshGraphVisualiser _visualiser;
        #endregion

        #region Public Methods
        /// <summary>
        /// Converts nodes to world coordinates
        /// </summary>
        /// <param name="nodeX"></param>
        /// <param name="nodeY"></param>
        /// <returns></returns>
        public Vector3 ConvertNodeToWorldCoordinates(int nodeX, int nodeY)
        {
            var x = WorldOrigin.x + 2f * NodeRadius * nodeX + NodeRadius;
            var y = WorldOrigin.y + 2f * NodeRadius * nodeY + NodeRadius;
            var h = -MeshGraphVisualiser.MaxMeshHeight - 0.5f;
            return new Vector3(x, Is2dMode  ? y : h, Is2dMode  ? h : y);
        }

        /// <summary>
        /// Converts a world position to a node x and y coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Node ConvertWorldCoordinatesToNode(Vector3 worldPos)
        {
            var percentX = Mathf.Clamp01((worldPos.x - WorldOrigin.x) / WorldSize.x);
            var percentY = Mathf.Clamp01((worldPos.y - WorldOrigin.y) / WorldSize.y);

            int x = Mathf.FloorToInt((_nodesInX - 1f) * percentX);
            int y = Mathf.FloorToInt((_nodesInY - 1f) * percentY);

            return _grid[x, y];
        }

        /// <summary>
        /// Gets the neighbours of a given node
        /// </summary>
        /// <param name="home"></param>
        /// <returns></returns>
        public List<Node> GetNodeNeighbours(Node home)
        {
            var results = new List<Node>();

            for (var i = -1; i <= 1; ++i)
            {
                for (var j = -1; j <= 1; ++j)
                {
                    if (i == 0 && j == 0) continue;

                    var checkX = home.X + i;
                    var checkY = home.Y + j;

                    if (checkX >= 0 && checkX < _nodesInX
                        && checkY >=0 && checkY < _nodesInY)
                    {
                        results.Add(_grid[checkX, checkY]);
                    }
                }
            }

            return results;
        }
        
        /// <summary>
        /// Gets a path synchronously
        /// </summary>
        /// <param name="start"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public List<Vector3> GetPathSynchronous(Vector3 start, Vector3 target)
        {
            return _pathFinder.FindPath(this, start, target);
        }

        /// <summary>
        /// Gets a path for a given navigation agent asynchronously
        /// </summary>
        /// <param name="start"></param>
        /// <param name="target"></param>
        public void GetPathAsync(Vector3 start, Vector3 target, INavigationAgent agent)
        {
            var j = new PathfindingThreadedJob(this, start, target, agent);
            _jobQueue.AddJob(j);
        }

        /// <summary>
        /// Gets a path for a given object (usually a component) asynchronously and when it
        /// is found returns it on the message bus. The PathfindingRequestArgs should be populated
        /// </summary>
        /// <param name="start"></param>
        /// <param name="target"></param>
        public void GetPathAsync(PathfindingRequestArgs args)
        {
            var j = new PathfindingThreadedJob(args);
            _jobQueue.AddJob(j);
        }

        /// <summary>
        /// Checks line of sight from point A to point B depending on the terrain
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public bool InLineOfSight(Vector3 from, Vector3 to)
        {
            // get our target nodes
            var fromNode = ConvertWorldCoordinatesToNode(from);
            var toNode = ConvertWorldCoordinatesToNode(to);

            return InLineOfSight(_heightMap, fromNode, toNode);
        }

        /// <summary>
        /// Determine if the given points are within line of sight by tracing a line between the two
        /// and checking all points in the height map between the two points are lower than this the
        /// line between the two points.
        /// </summary>
        /// <param name="from">The world coordinates of the viewer</param>
        /// <param name="to">The world coordinates of the target</param>
        /// <returns></returns>
        public static bool InLineOfSight(byte[,] heightMap, Node fromNode, Node toNode)
        {
            // put our nodes the right way around so we move "up" the x line rather than down it
            // (i.e. ensure we move x_from < x_to). Also handle the case where from.X == to.X which
            // requires we traverse in Y
            var deltaX = (toNode.X - fromNode.X);
            var deltaY = (toNode.Y - fromNode.Y);

            var inY = deltaY > deltaX;

            if ((!inY && fromNode.X > toNode.X) ||
                (inY && fromNode.Y > toNode.Y))
            {
                var tmp = fromNode;
                fromNode = toNode;
                toNode = tmp;
            }

            // get a function for determining the next point to check
            var gradFunc = GetLineEquation(fromNode, toNode, inY);
            
            // find our target heights
            var fromHeight = heightMap[fromNode.X, fromNode.Y];
            var toHeight = heightMap[toNode.X, toNode.Y];

            // get our indices based on whether we are moving in the x or y axis
            var fromIdx = inY ? fromNode.Y : fromNode.X;
            var toIdx = inY ? toNode.Y : toNode.X;
            
            // setup some tracking variables
            var proportion = 0f;
            var deltaP = 1f / (toIdx - fromIdx);

            // traverse along the active axis, checking the nearest integer node for height
            // simple but possibly not 100% accurate all the time!
            for (var i = fromIdx; i < toIdx; ++i)
            {
                var j = gradFunc(i);

                if (IsValidHeight(fromHeight, toHeight, proportion, inY ? heightMap[j, i] : heightMap[i, j]) == false ||
                    IsValidHeight(fromHeight, toHeight, 1f - proportion, inY ? heightMap[j, i] : heightMap[i, j]) == false)
                {
                    return false;
                }

                proportion += deltaP;
            }

            return true;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Determines the gradient of a line defined by two points and returns a function 
        /// which takes a float x position and returns the closest int y position.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="inY"></param>
        /// <returns></returns>
        private static Func<float, int> GetLineEquation(Node point1, Node point2, bool inY)
        {
            var x1 = inY ? point1.Y : point1.X;
            var x2 = inY ? point2.Y : point2.X;
            var y1 = inY ? point1.X : point1.Y;
            var y2 = inY ? point2.X : point2.Y;

            var m = (float)(y2 - y1) / (x2 - x1);
            var c = y1 - m * x1;

            return (x) =>
            {
                return Mathf.RoundToInt(m * x + c);
            };
        }

        /// <summary>
        /// Checks if the current height is less than the target height
        /// </summary>
        /// <param name="startHeight"></param>
        /// <param name="endHeight"></param>
        /// <param name="proportionAlong"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        private static bool IsValidHeight(float startHeight, float endHeight, float proportionAlong, float current)
        {
            var targetHeight = (startHeight - endHeight) * proportionAlong + endHeight;
            return current <= targetHeight;
        }

        /// <summary>
        /// Sets up the graph using injected dependencies
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="pathFinder"></param>
        /// <param name="jobQueue"></param>
        [PostInject]
        private void Init(GraphBuilderFactory factory, IPathFinder pathFinder, AsynchronousJobQueue jobQueue, MeshGraphVisualiser visualiser)
        {
            _factory = factory;
            _pathFinder = pathFinder;
            _jobQueue = jobQueue;
            _visualiser = visualiser;

            Subscribe();
        }

        /// <summary>
        /// Builds a graph with the given parameters and returns a new game object containing the graph
        /// </summary>
        /// <param name="builderType"></param>
        /// <param name="unwalkableLayer"></param>
        /// <param name="graphDefinition"></param>
        /// <param name="buildMesh"></param>
        /// <returns></returns>
        public void BuildGraph(GraphBuilderType builderType, 
            LayerMask unwalkableLayer, 
            Transform parent, 
            float nodeRadius, 
            string graphDefinition, 
            bool buildMesh, 
            bool is2d,
            Vector2 worldSize,
            Vector3 worldOrigin)
        {
            // save attributes
            NodeRadius = nodeRadius;
            WorldOrigin = worldOrigin;
            Is2dMode = is2d;
            WorldSize = worldSize;

            // calculate the number of x and y nodes
            _nodesInX = Mathf.FloorToInt(WorldSize.x / (2f * NodeRadius));
            _nodesInY = Mathf.FloorToInt(WorldSize.y / (2f * NodeRadius));

            // setup the graph
            var builder = _factory.Get(builderType);
            builder.Configure(unwalkableLayer, graphDefinition);
            _grid = builder.BuildGraph(_nodesInX, _nodesInY, NodeRadius, Is2dMode, ConvertNodeToWorldCoordinates);
            _heightMap = builder.BuildHeightMap(_nodesInX, _nodesInY);
            
            // check if we are building a mesh from the heightmap
            if (buildMesh)
            {
                var gameObj = _visualiser.GenerateMesh(_heightMap, nodeRadius * 2, nodeRadius * 2);
                gameObj.transform.SetParent(parent);
            }
        }

        /// <summary>
        /// Listen for pathfinding requests
        /// </summary>
        public void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.PathRequested);
        }

        /// <summary>
        /// Handle pathfinding request
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.PathRequested)
            {
                var prargs = args.Get<PathfindingRequestArgs>();
                prargs.Graph = this;
                GetPathAsync(prargs);
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the size of the World in X/Y pixels
        /// </summary>
        public Vector2 WorldSize { get; private set; }

        /// <summary>
        /// Gets a flag indicating if the graph is in 2d mode
        /// </summary>
        public bool Is2dMode { get; private set; }

        /// <summary>
        /// Gets the radius of nodes in the graph in game pixels
        /// </summary>
        public float NodeRadius { get; private set; }

        /// <summary>
        /// Gets or sets the world origin point
        /// </summary>
        public Vector3 WorldOrigin { get; private set; }

        /// <summary>
        /// Gets the 2d heightmap array for this level
        /// </summary>
        public byte[,] HeightMap
        {
            get
            {
                return _heightMap;
            }
        }

        /// <summary>
        /// Gets the grid 
        /// </summary>
        public Node[,] Grid
        {
            get
            {
                return _grid;
            }
        }
        #endregion
    }
}