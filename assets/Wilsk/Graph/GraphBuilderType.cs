﻿namespace Wilsk.Graph
{
    public enum GraphBuilderType
    {
        Empty = 0,
        Raycast = 1,
        String = 2,
        File = 3,
        UnityResource = 4
    }
}
