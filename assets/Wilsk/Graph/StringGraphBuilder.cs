﻿namespace Wilsk.Graph
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class StringGraphBuilder : IGraphBuilder
    {
        /// <summary>
        /// The graph parser to use for generating graphs
        /// </summary>
        private GraphParser _parser;

        /// <summary>
        /// Builds a graph from the string input
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <param name="nodeRadius"></param>
        /// <param name="is2dMode"></param>
        /// <param name="getWorldPosition"></param>
        /// <returns></returns>
        public Node[,] BuildGraph(int sizeX, int sizeY, float nodeRadius, bool is2dMode, Func<int, int, Vector3> getWorldPosition)
        {
            if (_parser.GridHeight != sizeY || _parser.GridWidth != sizeX)
            {
                Debug.LogWarningFormat("Possible issue with loaded graph, expected size ({0},{1}) doesn't match graph size ({2},{3})", sizeX, sizeY, _parser.GridHeight, _parser.GridHeight);
            }

            return _parser.BuildGraph(nodeRadius, is2dMode, getWorldPosition);
        }

        /// <summary>
        /// Builds a height map from the string input
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <returns></returns>
        public byte[,] BuildHeightMap(int sizeX, int sizeY)
        {
            if (_parser.GridHeight != sizeY || _parser.GridWidth != sizeX)
            {
                Debug.LogWarningFormat("Possible issue with loaded heightmap, expected size ({0},{1}) doesn't match graph size ({2},{3})", sizeX, sizeY, _parser.GridHeight, _parser.GridHeight);
            }

            return _parser.BuildHeightMap();
        }
        
        /// <summary>
        /// For the empty graph builder, this doesn't do anything - it just returns
        /// an empty grid
        /// </summary>
        /// <param name="inputData"></param>
        public virtual void Configure(LayerMask unwalkable, string fileText)
        {
            _parser = new GraphParser(fileText);
        }
    }
}
