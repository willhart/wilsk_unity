﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides an abstract base class for path finding, including the mechanism for 
    /// throttled, asynchronous (threaded) path generation
    /// </summary>
    public abstract class AbstractPathFinder : IPathFinder
    {
        public abstract List<Vector3> FindPath(Graph grid, Vector3 startPos, Vector3 targetPos);

        public List<Vector3> FindPath(PathfindingRequestArgs args)
        {
            return FindPath(args.Graph, args.Origin, args.Destination);
        }

        /// <summary>
        /// Simplifies a path removing irrelevant vertices
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected List<Vector3> SimplifyPath(List<Node> path)
        {
            List<Vector3> waypoints = new List<Vector3>() { path[0].WorldPosition };
            Vector2 dir = Vector2.zero;

            for (var i = 1; i < path.Count; ++i)
            {
                var newDir = new Vector2(path[i - 1].X - path[i].X, path[i-1].Y - path[i].Y);

                if (dir != newDir)
                {
                    waypoints.Add(path[i].WorldPosition);
                }

                dir = newDir;
            }

            // check we've added the destination node
            if (waypoints[waypoints.Count - 1] != path[path.Count - 1].WorldPosition)
            {
                waypoints.Add(path[path.Count - 1].WorldPosition);
            }

            return waypoints;
        }
    }
}
