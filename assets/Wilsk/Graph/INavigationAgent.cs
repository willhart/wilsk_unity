﻿namespace Wilsk.Graph
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface INavigationAgent
    {
        void SetWaypoints(List<Vector3> waypoints);
        Vector3 GetNextWaypoint(Vector3 currentPosition);
    }
}
