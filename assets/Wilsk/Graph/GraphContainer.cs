﻿namespace Wilsk.Graph
{
    using UnityEngine;
    using Zenject; 

    /// <summary>
    /// A simple container class for injecting a graph into a scene
    /// </summary>
    public class GraphContainer : MonoBehaviour
    {
        #region Private Members
        [Inject]
        private Graph _graph;
        #endregion
        
        #region Unity Members
        /// <summary>
        /// Holds the size in unity units of the grid in the x and y direction.
        /// This is not the same as the number of x and y nodes
        /// </summary>
        [SerializeField]
        private Vector2 _worldSize = new Vector2(30, 30);

        /// <summary>
        /// A flag indicating whether the grid is operating in 2d (x,y) or 3d (x,z) mode
        /// </summary>
        [SerializeField]
        private bool _is2dMode = true;

        /// <summary>
        /// The radius of individual nodes in the world
        /// </summary>
        [SerializeField]
        private float _nodeRadius = 0.5f;

#pragma warning disable 0649
        /// <summary>
        /// Specify unwalkable layers in the grid to be built (used for raycast graph builders)
        /// </summary>
        [SerializeField]
        private LayerMask _unwalkableLayer;

        /// <summary>
        /// The definition for the graph - either the graph string itself or a path name or empty 
        /// </summary>
        [SerializeField, Multiline(10)]
        private string _graphDefinition;
#pragma warning restore 0649

        /// <summary>
        /// When set to true uses a MeshGraphVisualiser to build up a mesh for the loaded heightmap
        /// </summary>
        [SerializeField]
        private bool _buildMeshForGraph = false;

        /// <summary>
        /// The graph builder to use to generate the grid
        /// </summary>
        [SerializeField]
        private GraphBuilderType _graphBuilder = GraphBuilderType.Empty;
        #endregion

#if UNITY_EDITOR
        #region Debugging
        /// <summary>
        /// Draws the world grid
        /// </summary>
        void OnDrawGizmos()
        {
            if (_graph == null) return;

            // calculate the game object as "bottom left" of grid, handling 2d or 3d modes
            var wirePos = new Vector3(transform.position.x + _graph.WorldSize.x / 2,
                transform.position.y + (_graph.Is2dMode ? _graph.WorldSize.y / 2 : 0),
                transform.position.z + (_graph.Is2dMode ? 0 : _graph.WorldSize.y / 2));

            Gizmos.DrawWireCube(wirePos,
                _graph.Is2dMode ?
                    new Vector3(_graph.WorldSize.x, _graph.WorldSize.y, -1) :
                    new Vector3(_graph.WorldSize.x, 1, _graph.WorldSize.y));

            // draw the generated grid
            if (_graph.Grid != null)
            {
                foreach (var n in _graph.Grid)
                {
                    Gizmos.color = n.Walkable ? Color.white : Color.red;
                    Gizmos.DrawCube(n.WorldPosition, Vector3.one * _graph.NodeRadius * 1.95f);
                }
            }
        }
        #endregion
#endif

        #region Private Methods
        [PostInject]
        private void Init()
        {
            _graph.BuildGraph(_graphBuilder, _unwalkableLayer,
                transform, _nodeRadius, _graphDefinition, _buildMeshForGraph,
                _is2dMode, _worldSize, transform.position);
        }
        #endregion
    }
}
