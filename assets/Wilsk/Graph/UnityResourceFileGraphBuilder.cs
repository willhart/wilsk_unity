﻿namespace Wilsk.Graph
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Load graph data from a file and build heightmap and graph
    /// </summary>
    public class UnityResourceFileGraphBuilder : StringGraphBuilder
    {
        /// <summary>
        /// Configures the file builder
        /// </summary>
        /// <param name="unwalkable"></param>
        /// <param name="fileText"></param>
        public override void Configure(LayerMask unwalkable, string filePath)
        {
            var data = (TextAsset)Resources.Load(filePath, typeof(TextAsset));

            if (data == null)
            {
                throw new NullReferenceException("Unable to load in the level data");
            }

            base.Configure(unwalkable, data.text);
        }
    }
}
