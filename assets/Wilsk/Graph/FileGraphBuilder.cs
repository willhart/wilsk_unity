﻿namespace Wilsk.Graph
{
    using System.IO;
    using UnityEngine;

    /// <summary>
    /// Load graph data from a file and build heightmap and graph
    /// </summary>
    public class FileGraphBuilder : StringGraphBuilder
    {
        /// <summary>
        /// Configures the file builder
        /// </summary>
        /// <param name="unwalkable"></param>
        /// <param name="fileText"></param>
        public override void Configure(LayerMask unwalkable, string filePath)
        {
            var data = File.ReadAllText(filePath);
            base.Configure(unwalkable, data);
        }
    }
}
