﻿namespace Wilsk.Entities
{
    using System;
    using UnityEngine;

    using Components;
    using Core.Messaging;
    using Graph;
    using Input;

    /// <summary>
    /// A class designed to group together all the interactions an entity might have and 
    /// control their activities using a ComponentMessageBus.
    /// 
    /// Generally shouldn't be injected, but install using the RequireComponent attribute
    /// 
    /// Currently only set up for 2D
    /// </summary>
    public class EnemyInteractionComponent : MonoBehaviour, IMessageBusSubscriber
    {
        /// <summary>
        /// A reference to the message bus used for internal communication
        /// </summary>
        private ComponentMessageBus _bus;

        /// <summary>
        /// The enemy component bundle this component interacts with
        /// </summary>
        private EnemyComponentBundle _enemy;
                                
        /// <summary>
        /// Tidy up resources on desctruction
        /// </summary>
        void Destroy()
        {
            _enemy.Died -= EntityDied;
            _enemy.Moved -= HandleEnemyMessage;
        }

        /// <summary>
        /// Set up the interaction component, linking in events and caching refernces
        /// to enemy component bundles. MUST be called for any interaction
        /// via a game interface to be possible
        /// </summary>
        /// <param name="enemy"></param>
        public void Init(EnemyComponentBundle enemy, EntitySettings.EnemyTemplate template)
        {
            _bus = enemy.Bus;
            _enemy = enemy;

            _enemy.Died += EntityDied;
            _enemy.Moved += HandleEnemyMessage;

            Subscribe();

            _bus.SendMessage(this, MessageBusMessageType.RendererRequested, gameObject);
        }

        /// <summary>
        /// Handle events raised by the component bundle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleEnemyMessage(object sender, MessageBusEventArgs e)
        {
            if (e.Message == MessageBusMessageType.ComponentPositionUpdated)
            {
                transform.position = e.Get<Vector3>();
            }
        }

        /// <summary>
        /// Destroys the game object when it dies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EntityDied(object sender, EventArgs e)
        {
            Destroy(gameObject);
        }

        /// <summary>
        /// Selects/deselets the enemy, if a Selectable component is installed
        /// </summary>
        public void OnMouseDown()
        {
            _bus.SendMessage(this, MessageBusMessageType.ComponentOnLeftClick, null);
        }

        /// <summary>
        /// Subscribe to messages on the bus
        /// </summary>
        public void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentOnRightClick);
        }

        /// <summary>
        /// Handle received messages
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ComponentOnRightClick)
            {
                if ((_enemy.GetComponent(ComponentType.Selectable) as SelectableComponent).IsSelected)
                {
                    var pos = Camera.main.ScreenToWorldPoint(args.Get<InputEventArgs>().Location);

                    // prevent "null" waypoint @ {0,0,0}
                    if (pos == Vector3.zero)
                    {
                        pos = new Vector3(0.001f, 0.001f, 0.001f); 
                    }

                    _bus.SendMessage(this, MessageBusMessageType.ComponentSetNewDestination, pos);
                }
            }
        }
    }
}
