﻿namespace Wilsk.Entities
{
    using System;

    [Serializable]
    public class EntityStats
    {
        public float MovementSpeed;

        public float Damage;

        public float Stealth;

        public float StartingHealth;

        public float SightRange;

        /// <summary>
        /// Default constructor sets default stats values
        /// </summary>
        public EntityStats()
        {
            // Set defaults
            MovementSpeed = 5;
            Damage = 5;
            Stealth = 5;
            StartingHealth = 100;
        }

        /// <summary>
        /// Default constructor copying values from template
        /// </summary>
        /// <param name="original"></param>
        public EntityStats(EntityStats original)
        {
            Merge(original);
        }

        /// <summary>
        /// Merge in another stats class into this one
        /// </summary>
        /// <param name="other"></param>
        public void Merge(EntityStats other)
        {
            MovementSpeed += other.MovementSpeed;
            Damage += other.Damage;
            Stealth += other.Stealth;
            StartingHealth += other.StartingHealth;
        }
    }
}
