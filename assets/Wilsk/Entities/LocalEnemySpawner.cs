﻿namespace Wilsk.Entities
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    public class LocalEnemySpawner : MonoBehaviour
    {
        #region Debugging
        IEnumerator PeriodicSpawn()
        {
            while (true)
            {
                yield return new WaitForSeconds(5);
                Spawn(Vector3.zero, EnemyType.Infantry);
            }
        }
        #endregion

        #region Private Members
        /// <summary>
        /// An injected reference to the factory for spawning new enemies
        /// </summary>
        private LocalEnemy.Factory _factory;

        /// <summary>
        /// A list of currently spawned enemies
        /// </summary>
        private readonly List<LocalEnemy> _enemies = new List<LocalEnemy>();
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="factory"></param>
        [PostInject]
        public void Setup(LocalEnemy.Factory factory)
        {
            _factory = factory;

            StartCoroutine(PeriodicSpawn());
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Spawn a new enemy
        /// </summary>
        public void Spawn(Vector3 location, EnemyType enemyToSpawn)
        {
            var enemy = _factory.Create(enemyToSpawn, location);
            _enemies.Add(enemy);
        }
        #endregion
    }
}
