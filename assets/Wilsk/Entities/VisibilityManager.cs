﻿namespace Wilsk.Entities
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Zenject;

    using Components;
    using Core.Messaging;
    using Core;
    using Graph;

    /// <summary>
    /// Manages the game wide visibility of objects
    /// </summary>
    public class VisibilityManager : IMessageBusSubscriber
    {
        /// <summary>
        /// The update period (in seconds) for the visibility manager
        /// </summary>
        private static readonly float UpdateFrequency = 5f;

#pragma warning disable 0649
        /// <summary>
        /// The global message bus used for getting component registration requests
        /// </summary>
        [Inject]
        private MessageBus _bus;
        
        /// <summary>
        /// The graph to use for checking line of sight
        /// </summary>
        [Inject]
        private Graph _graph;
#pragma warning restore 0649

        /// <summary>
        /// Gets the next update time
        /// </summary>
        private float _nextUpdateTime = 0;
        
        /// <summary>
        /// The visiblity components being managed
        /// </summary>
        private readonly HashSet<VisibilityComponent> _components = new HashSet<VisibilityComponent>();

        /// <summary>
        /// The teams in the game
        /// </summary>
        private readonly HashSet<int> _teams = new HashSet<int>();
        
        /// <summary>
        /// Handles registration and triggering of updates for visibility components
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            if (type == MessageBusMessageType.ComponentRegisterForVisibility)
            {
                var c = args.Sender as VisibilityComponent;
                if (c != null)
                {
                    Register(c);
                    _teams.Add(c.Team);
                }
            }
        }

        /// <summary>
        /// Subscribe to messages on the global bus
        /// </summary>
        public void Subscribe()
        {
            _bus.Subscribe(this, MessageBusMessageType.ComponentRegisterForVisibility);
        }

        /// <summary>
        /// Sets the graph to use for Line of Sight checks
        /// </summary>
        /// <param name="g"></param>
        public void SetGraph(Graph g)
        {
            _graph = g;
        }

        /// <summary>
        /// Periodically update the visibility of components
        /// </summary>
        public void Update()
        {
            if (TimeKeeper.GameTime < _nextUpdateTime || TimeKeeper.Paused) return;

            //var t1 = System.DateTime.Now;

            foreach (var component in _components)
            {
                SetComponentVisibility(component);
            }
            
            _nextUpdateTime = TimeKeeper.GameTime + UpdateFrequency;

            //Debug.Log(string.Format("Updated visibility in {0}ms", (System.DateTime.Now - t1).Milliseconds));
        }

        /// <summary>
        /// Register a component and set it's visibliity state to dirty
        /// </summary>
        /// <param name="component"></param>
        private void Register(VisibilityComponent component)
        {
            if (!_components.Contains(component))
            {
                _components.Add(component);
            }
        }

        /// <summary>
        /// Checks the visibility of a single component
        /// </summary>
        /// <param name="component"></param>
        private void SetComponentVisibility(VisibilityComponent component)
        {
            // manage the teams we are checking for
            var teams = new HashSet<int>(_teams);
            teams.Remove(component.Team);

            teams.ToList().ForEach(t =>
            {
                component.SetVisibleTo(t, false);
            });

            // check against all enemies until we have checked for all teams
            foreach (var enemy in _components)
            {
                if (teams.Count == 0) break; // have we checked for all teams
                if (!teams.Contains(enemy.Team)) continue; // have we checked for this team
                
                // check range
                if (Vector3.Distance(enemy.Position, component.Position) < enemy.SightRange)
                {
                    // check LOS if a graph has been provided
                    if (_graph == null || _graph.InLineOfSight(enemy.Position, component.Position))
                    {
                        component.SetVisibleTo(enemy.Team, true);
                        teams.Remove(enemy.Team);
                    }
                }
            }
        }
    }
}
