﻿namespace Wilsk.Entities
{
    using System;
    using UnityEngine;

    using Core;
    using Components;

    [Serializable]
    public class EntitySettings
    {
        public GameObject BaseEnemyPrefab;

        public EnemyTemplate[] EnemyTemplates;
        
        [Serializable]
        public class EnemyTemplate
        {
            public EnemyType EnemyType;

            public string Name;

            [EnumFlag]
            public ComponentType Components;

            public Sprite EnemySprite;

            public EntityStats Stats;

            public int Team;

            public override string ToString()
            {
                return Name + "(" + EnemyType.ToString() + ")";
            }
        }
    }
}
