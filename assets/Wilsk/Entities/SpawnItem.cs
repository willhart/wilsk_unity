﻿namespace Wilsk.Entities
{
    using System;
    using UnityEngine;

    /// <summary>
    /// A description of an inspector-editable spawn
    /// </summary>
    [Serializable]
    public class SpawnItem
    {

#pragma warning disable 0649
        /// <summary>
        /// The type of enemy to spawn
        /// </summary>
        [SerializeField]
        private EnemyType _enemyType;

        /// <summary>
        /// The minimum number of enemies to spawn
        /// </summary>
        [SerializeField]
        private int MinQuantity;

        /// <summary>
        /// The maximum number of enemies to spawn	
        /// </summary>
        [SerializeField]
        private int MaxQuantity;
#pragma warning restore 0649

        /// <summary>
        /// Gets the quantity of enemies to spawn
        /// </summary>
        /// <value>The quantity.</value>
        public int Quantity
        {
            get
            {
                return UnityEngine.Random.Range(this.MinQuantity, this.MaxQuantity + 1);
            }
        }

        /// <summary>
        /// Gets the enemy to spawn
        /// </summary>
        /// <value>The EnemyType to spawn.</value>
        public EnemyType Enemy
        {
            get
            {
                return _enemyType;
            }
        }
    }

}
