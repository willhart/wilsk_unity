﻿namespace Wilsk.Entities
{
    public enum SpawnCompletionType
    {
        AllEnemiesDead = 0,
        Timed = 1
    }

}
