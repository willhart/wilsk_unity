﻿namespace Wilsk.Entities
{
    using System;
    using System.Collections.Generic;

    using Components;
    using Core.Messaging;
    using Core;
    using UnityEngine;
    using Graph;

    /// <summary>
    /// Holds the logic for enemies regardless of whether they are local or networked
    /// If the class is local, then this should be injected using Zenject. If networked,
    /// it should be directly instantiated with the new objects passed to the constructor.
    /// 
    /// The Update() method needs to be called explicitly
    /// </summary>
    public class EnemyComponentBundle : AbstractComponentBundle
    {
        #region Events
        /// <summary>
        /// Triggered when the game object should be removed
        /// </summary>
        public event EventHandler Died;

        /// <summary>
        /// Triggered when the game object moves
        /// </summary>
        public event EventHandler<MessageBusEventArgs> Moved;
        #endregion

        #region Private Members
        /// <summary>
        /// Store a reference to the stats component for convenience
        /// </summary>
        private EntityStats _stats;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public EnemyComponentBundle(ComponentFactory factory, ComponentMessageBus componentBus, MessageBus globalBus) 
            : base(factory, componentBus, globalBus) { }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets up the enemy and applies stats modifiers
        /// </summary>
        /// <param name="template"></param>
        /// <param name="modifiers"></param>
        internal void SetupEnemy(EntitySettings.EnemyTemplate template, MessageBus globalBus, IEnumerable<EntityStats> modifiers)
        {
            _stats = template.Stats;
            SetupEnemy(template, globalBus);
            ApplyModifiers(modifiers);
        }

        /// <summary>
        /// Set up the enemy to use the correct template. Assumes the derived class has already
        /// set up the dependencies - i.e. through injection locally or instantiation in networked enemies
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="settingsId"></param>
        internal void SetupEnemy(EntitySettings.EnemyTemplate template, MessageBus globalBus)
        {
            BuildComponents(template);
        }

        /// <summary>
        /// Applies a collection of stats modifiers
        /// </summary>
        /// <param name="modifiers"></param>
        internal void ApplyModifiers(IEnumerable<EntityStats> modifiers)
        {
            foreach (var modifier in modifiers)
            {
                Stats.Merge(modifier);
            }
        }

        /// <summary>
        /// Applies a single stats modifier
        /// </summary>
        /// <param name="modifier"></param>
        internal void ApplyModifiers(EntityStats modifier)
        {
            Stats.Merge(modifier);
        }

        /// <summary>
        /// Called periodically (usually each frame) by a MonoBehaviour to update enemy state
        /// </summary>
        internal void Update()
        {
            foreach (var component in _loadedComponents.Values)
            {
                component.Update();
            }
        }

        /// <summary>
        /// Subscribe to component message bus messages
        /// </summary>
        public override void Subscribe()
        {
            base.Subscribe();
            _bus.Subscribe(this, MessageBusMessageType.DestroyGameObject);
            _bus.Subscribe(this, MessageBusMessageType.ComponentPositionUpdated);
            _bus.Subscribe(this, MessageBusMessageType.PathRequested);
            _bus.Subscribe(this, MessageBusMessageType.PathAvailable);
            _bus.Subscribe(this, MessageBusMessageType.ComponentRegisterForVisibility);
        }

        /// <summary>
        /// Handle incoming 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args"></param>
        public override void HandleMessage(MessageBusMessageType type, MessageBusEventArgs args)
        {
            base.HandleMessage(type, args);

            switch (type)
            {
                case MessageBusMessageType.DestroyGameObject:
                    if (Died != null)
                    {
                        Died(this, new EventArgs());
                    }
                    break;

                case MessageBusMessageType.ComponentPositionUpdated:
                    if (Moved != null)
                    {
                        Moved(this, args);
                    }
                    break;

                case MessageBusMessageType.PathRequested:
                    _globalBus.SendMessage(this, MessageBusMessageType.PathRequested, args.Get<PathfindingRequestArgs>());
                    break;

                case MessageBusMessageType.PathAvailable:
                    _bus.SendMessage(this, MessageBusMessageType.ComponentWaypointsUpdated, args.Get<List<Vector3>>());
                    break;

                case MessageBusMessageType.ComponentRegisterForVisibility:
                    _globalBus.SendMessage(args.Sender, MessageBusMessageType.ComponentRegisterForVisibility, null);
                    break;

                default:
                    Debug.LogWarning("Unexpected message type received on component bundle - " + type.ToString());
                    break;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets a reference to the enemy stats implementation
        /// </summary>
        public EntityStats Stats
        {
            get
            {
                return _stats;
            }
        }

        /// <summary>
        /// Gets a reference to a component bus used for internal messaging
        /// </summary>
        public ComponentMessageBus Bus
        {
            get
            {
                return _bus;
            }
        }
        #endregion
    }
}