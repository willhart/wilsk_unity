﻿namespace Wilsk.Entities
{
    using System;
    using UnityEngine;
    using Core.Messaging;
    using Zenject;

    /// <summary>
    /// Injects dependencies for a local enemy
    /// </summary>
    [RequireComponent(typeof(EnemyInteractionComponent))]
    public class LocalEnemy : MonoBehaviour
    {
        #region Events
        /// <summary>
        /// Passed on from the BaseEnemy Died event when health is reduced to 0
        /// </summary>
        public event EventHandler Died;
        #endregion

        #region Private Members
#pragma warning disable 0649
        /// <summary>
        /// Handles enemy logic
        /// </summary>
        [Inject]
        private EnemyComponentBundle _enemy;

        /// <summary>
        /// The global message bus
        /// </summary>
        [Inject]
        private MessageBus _globalBus;
#pragma warning restore 0649
        #endregion

        #region Unity Methods
        void Update()
        {
            _enemy.Update();
        }

        #endregion

        #region Private Methods
        [PostInject]
        private void SetupEnemy(EntitySettings settings, EnemyType settingsId, Vector3 position)
        {
            // get the enemy template
            if (settings.EnemyTemplates.Length == 0)
            {
                Debug.LogError("Unable to spawn enemy with template as no templates are set up");
                return;
            }

            EntitySettings.EnemyTemplate template;

            try
            {
                template = settings.EnemyTemplates[(int)settingsId];
            }
            catch (IndexOutOfRangeException)
            {
                Debug.LogError("Index out of range exception when spawning an enemy, are there enough enemy templates?");
                template = settings.EnemyTemplates[0];
            }

            // setup the enemy
            _enemy.SetupEnemy(template, _globalBus);

            // enable interaction
            GetComponent<EnemyInteractionComponent>().Init(_enemy, template);

            // set the starting position
            _enemy.Bus.SendMessage(this, MessageBusMessageType.ComponentSetInitialPosition, position);
            transform.position = position;
        }
        #endregion

        #region Spawn Factory
        public class Factory : GameObjectFactory<EnemyType, Vector3, LocalEnemy> { }
        #endregion
    }
}