﻿namespace Wilsk.Entities
{
    /// <summary>
    /// Specify different enemy types by name. The number
    /// of elements here must match the number of elements specified in the settings object
    /// on the EntityDependencyInstaller on the composition root.
    /// </summary>
    public enum EnemyType
    {
        Infantry = 0,
        Motorised,
        Mechanised,
        Armoured,
        AirCavalry,
        Airborne,
        Helicopter,
        Jet,
        FOB,
        Firebase,
    }
}
