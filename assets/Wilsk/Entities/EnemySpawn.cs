﻿namespace Wilsk.Entities
{
    using UnityEngine;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Zenject;

    [RequireComponent(typeof(SphereCollider))]
    public class EnemySpawn : MonoBehaviour
    {
        #region Events
        /// <summary>
        /// Triggered when the spawn is completed (i.e. the time has expired or enemies are dead)
        /// </summary>
        public event EventHandler SpawnComplete;
        #endregion

        #region Unity Properties
        /// <summary>
        /// The list of spawn items to generate for this spawn point
        /// </summary>
#pragma warning disable 0649
        [SerializeField]
        SpawnItem[] _spawnItems;
#pragma warning restore 0649

        /// <summary>
        /// The range of the trigger	
        /// /// </summary>
        [SerializeField]
        float _triggerRange = 40f;

        /// <summary>
        /// The type of completion condition for this spawn
        /// </summary>
        [SerializeField]
        SpawnCompletionType _completionType = SpawnCompletionType.AllEnemiesDead;
        #endregion

        #region System Properties
        /// <summary>
        /// A flag to indicate if the spawn has been triggered
        /// </summary>
        private bool _triggered = false;

        /// <summary>
        /// The sphere collider which triggers the spawn
        /// </summary>
        private SphereCollider _sphereCollider;

        /// <summary>
        /// The enemies that were spawned by the spawn point
        /// </summary>
        private readonly List<LocalEnemy> _spawnedEnemies = new List<LocalEnemy>();

#pragma warning disable 0649
        /// <summary>
        /// A Factory to use for spawning enemies
        /// </summary>
        [Inject]
        LocalEnemy.Factory _factory;
#pragma warning restore 0649
        #endregion

        /// <summary>
        /// Start this instance and set up the collider trigger
        /// </summary>
        void Awake()
        {
            this._sphereCollider = GetComponent<SphereCollider>();
            this._sphereCollider.radius = _triggerRange;
            this._sphereCollider.transform.position = transform.position;
            this._sphereCollider.isTrigger = true;
        }

        /// <summary>
        /// Handles the On Trigger Enter to set up the spawn point
        /// </summary>
        public void OnTriggerEnter()
        {
            this.DoTrigger();
        }

        /// <summary>
        /// Completes the trigger action by creating a new enemy spawn. 
        /// Only trigger on the master client
        /// </summary>
        public void DoTrigger()
        {
            if (!_triggered)
            {
                _triggered = true;
                _sphereCollider.enabled = false;

                // space out new unit creation
                StartCoroutine(SpawnItemsDelayed());
            }
        }

        /// <summary>
        /// Spaces out unit spawns to prevent a big lag when first spawning
        /// </summary>
        /// <returns></returns>
        private IEnumerator SpawnItemsDelayed()
        {
            foreach (var si in _spawnItems)
            {
                var pf = si.Enemy;
                var qty = si.Quantity;
                for (var i = 0; i < qty; ++i)
                {
                    var entGO = _factory.Create(pf, Vector3.zero);

                    if (_completionType == SpawnCompletionType.AllEnemiesDead)
                    {
                        var ent = entGO.GetComponent<LocalEnemy>();
                        _spawnedEnemies.Add(ent);
                        ent.Died += SpawnedEnemyDied;
                    }

                    yield return new WaitForFixedUpdate();
                }
            }
        }

        /// <summary>
        /// Triggered when an enemy is dead 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnedEnemyDied(object sender, System.EventArgs e)
        {
            // tidy up
            var eb = (LocalEnemy)sender;
            eb.Died -= this.SpawnedEnemyDied;
            this._spawnedEnemies.Remove((LocalEnemy)sender);

            // check completion conditions
            if (_completionType == SpawnCompletionType.AllEnemiesDead && _spawnedEnemies.Count == 0)
            {
                var sc = SpawnComplete;
                if (sc != null)
                {
                    sc(this, new EventArgs());
                }
            }
        }
    }

}
