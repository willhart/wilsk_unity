#if !ZEN_NOT_UNITY3D

using System;
using ModestTree.Util;
using ModestTree.Util.Debugging;
using UnityEngine;

namespace Zenject
{
    [System.Diagnostics.DebuggerStepThrough]
    public class UnityEventManager : MonoBehaviour, ITickable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action ApplicationGainedFocus = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action ApplicationLostFocus = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action<bool> ApplicationFocusChanged = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action ApplicationQuit = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action ChangingScenes = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action DrawGizmos = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action<int> MouseButtonDown = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action<int> MouseButtonUp = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action LeftMouseButtonDown = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action LeftMouseButtonUp = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action MiddleMouseButtonDown = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action MiddleMouseButtonUp = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action RightMouseButtonDown = delegate { };
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action RightMouseButtonUp = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action<MouseWheelScrollDirections> MouseWheelMoved = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action MouseMove = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action ScreenSizeChanged = delegate { };

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        public event Action Started = delegate { };

        Vector3 _lastMousePosition;

        int _lastWidth;
        int _lastHeight;

        public bool IsFocused
        {
            get;
            private set;
        }

        void Start()
        {
            _lastWidth = Screen.width;
            _lastHeight = Screen.height;
            Started();
        }

        public void Tick()
        {
            int buttonLeft = 0;
            int buttonRight = 1;
            int buttonMiddle = 2;

            if (Input.GetMouseButtonDown(buttonLeft))
            {
                LeftMouseButtonDown();
                MouseButtonDown(0);
            }
            else if (Input.GetMouseButtonUp(buttonLeft))
            {
                LeftMouseButtonUp();
                MouseButtonUp(0);
            }

            if (Input.GetMouseButtonDown(buttonRight))
            {
                RightMouseButtonDown();
                MouseButtonDown(1);
            }
            else if (Input.GetMouseButtonUp(buttonRight))
            {
                RightMouseButtonUp();
                MouseButtonUp(1);
            }

            if (Input.GetMouseButtonDown(buttonMiddle))
            {
                MiddleMouseButtonDown();
                MouseButtonDown(2);
            }
            else if (Input.GetMouseButtonUp(buttonMiddle))
            {
                MiddleMouseButtonUp();
                MouseButtonUp(2);
            }

            if (_lastMousePosition != Input.mousePosition)
            {
                _lastMousePosition = Input.mousePosition;
                MouseMove();
            }

            var mouseWheelState = UnityUtil.CheckMouseScrollWheel();

            if (mouseWheelState != MouseWheelScrollDirections.None)
            {
                MouseWheelMoved(mouseWheelState);
            }

            if (_lastWidth != Screen.width || _lastHeight != Screen.height)
            {
                _lastWidth = Screen.width;
                _lastHeight = Screen.height;
                ScreenSizeChanged();
            }
        }

        void OnDestroy()
        {
            ChangingScenes();
        }

        void OnApplicationQuit()
        {
            ApplicationQuit();
        }

        void OnDrawGizmos()
        {
            DrawGizmos();
        }

        void OnApplicationFocus(bool newIsFocused)
        {
            if (newIsFocused && !IsFocused)
            {
                IsFocused = true;
                ApplicationGainedFocus();
                ApplicationFocusChanged(true);
            }

            if (!newIsFocused && IsFocused)
            {
                IsFocused = false;
                ApplicationLostFocus();
                ApplicationFocusChanged(false);
            }
        }
    }
}

#endif
